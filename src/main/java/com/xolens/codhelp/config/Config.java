/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.config;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.property.EntityList;
import com.xolens.codhelp.property.Enumeration;
import com.xolens.codhelp.writer.extjs.ExtApiWriter;
import com.xolens.codhelp.writer.extjs.ExtButtonWriter;
import com.xolens.codhelp.writer.extjs.ExtContentViewWriter;
import com.xolens.codhelp.writer.extjs.ExtContentWriter;
import com.xolens.codhelp.writer.extjs.ExtDetailWriter;
import com.xolens.codhelp.writer.extjs.ExtFormWriter;
import com.xolens.codhelp.writer.extjs.filtered.ExtColumnWriter;
import com.xolens.codhelp.writer.extjs.filtered.ExtFieldWriter;
import com.xolens.codhelp.writer.pglaravel.LaravelAppApiWriter;
import com.xolens.codhelp.writer.pglaravel.LaravelAppUtilWriter;
import com.xolens.codhelp.writer.pglaravel.LaravelTemplateWriter;
import com.xolens.codhelp.writer.pglaravel.factory.FactoryWriter;
import com.xolens.codhelp.writer.pglaravel.migration.MigrationViewWriter;
import com.xolens.codhelp.writer.pglaravel.migration.MigrationWriter;
import com.xolens.codhelp.writer.pglaravel.model.ModelViewWriter;
import com.xolens.codhelp.writer.pglaravel.model.ModelWriter;
import com.xolens.codhelp.writer.pglaravel.repository.RepositoryContractWriter;
import com.xolens.codhelp.writer.pglaravel.repository.RepositoryViewContractWriter;
import com.xolens.codhelp.writer.pglaravel.repository.RepositoryViewWriter;
import com.xolens.codhelp.writer.pglaravel.repository.RepositoryWriter;
import com.xolens.codhelp.writer.pglaravel.test.RepositoryTestWriter;
import com.xolens.codhelp.writer.pglaravel.test.RepositoryViewTestWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cedrick
 */
@SuppressWarnings("UseSpecificCatch")
public class Config {
    private static final Config DEFAULT_CONF = new Config();
    private static Config STATIC_CONF;
    static{
        InputStream inputstream;
        try {
            inputstream = "".getClass().getResourceAsStream("/rsc/default-config.json");
            JsonReader reader = new JsonReader(new InputStreamReader(inputstream, "UTF-8"));
            HashMap<String,String> map = new Gson().fromJson(reader, HashMap.class);
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> pair = (Map.Entry)it.next();
                DEFAULT_CONF.put(pair.getKey(), pair.getValue());
            }
        } catch (Exception ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
  
    private static EntityList ENTITIES = new EntityList();
    private static Map<String, Enumeration> ENUMS = new HashMap<>();
    private final HashMap<String, String> values = new HashMap<>();
    
    public Config() {
    }
    
    
    public static EntityList list() {
        return ENTITIES;
    }
    
    public static Entity entity(String key) {
        return ENTITIES.get(key);
    }
    
    public static Enumeration enumeration(String key) {
        return ENUMS.get(key);
    }
    
    public static void setList(Entity[] list) {
        ENTITIES.load(list);
    }
    
    public static void setList(List<Entity> list) {
        ENTITIES.load(list);
    }
    
    public static void loadModel(String modelPath) throws IOException{
        Gson gson = new Gson();
        InputStream inputstream = new FileInputStream(modelPath);
        JsonReader reader = new JsonReader(new InputStreamReader(inputstream, "UTF-8"));
        if (reader.hasNext()) {
            Entity[] entities = gson.fromJson(reader, Entity[].class);
            list().load(entities);
        }
    }
    public static void loadEnum(String enumPath) throws IOException{
        Gson gson = new Gson();
        InputStream inputstream = new FileInputStream(enumPath);
        JsonReader reader = new JsonReader(new InputStreamReader(inputstream, "UTF-8"));
        if (reader.hasNext()) {
            Enumeration[] enums = gson.fromJson(reader, Enumeration[].class);
            ENUMS.clear();
            for(Enumeration e: enums){
                ENUMS.put(e.name(), e);
            }
        }
    }
    
    public static void writePhpPackageFile() throws IOException{
        LaravelAppUtilWriter law = new LaravelAppUtilWriter();
        law.writeAll();
    }
    
    public static void writeAppApiFile() throws IOException{
        LaravelAppApiWriter law = new LaravelAppApiWriter();
        law.writeAll();
    }
    
    public static void writeMigrations() throws IOException{
        for(Entity e: ENTITIES.list.values()){
            MigrationViewWriter mvWriter = new MigrationViewWriter(e);
            MigrationWriter mWriter = new MigrationWriter(e);
            mvWriter.write();
            mWriter.write();
        }
    }
    
    public static void writeModels() throws IOException{
        for(Entity e: ENTITIES.list.values()){
            ModelWriter mWriter = new ModelWriter(e);
            ModelViewWriter mvWriter = new ModelViewWriter(e);
            mWriter.write();
            mvWriter.write();
        }
    }
    
    
    public static void writeFactories() throws IOException{
        for(Entity e: ENTITIES.list.values()){
            FactoryWriter fWriter = new FactoryWriter(e);
            fWriter.write();
        }
    }
    
    public static void writeRepositories() throws IOException{
        for(Entity e: ENTITIES.list.values()){
            RepositoryWriter rWriter = new RepositoryWriter(e);
            RepositoryViewWriter rvWriter = new RepositoryViewWriter(e);
            RepositoryContractWriter rcWriter = new RepositoryContractWriter(e);
            RepositoryViewContractWriter rcvWriter = new RepositoryViewContractWriter(e);
            rWriter.write();
            rvWriter.write();
            rcWriter.write();
            rcvWriter.write();
        }
    }
    
    public static void writeTests() throws IOException{
        for(Entity e: ENTITIES.list.values()){
            RepositoryTestWriter rtWriter = new RepositoryTestWriter(e);
            RepositoryViewTestWriter rvtWriter = new RepositoryViewTestWriter(e);
            rtWriter.write();
            rvtWriter.write();
        }
    }
    
    public static void writePhpTemplates() throws IOException{
        LaravelTemplateWriter licenceWr = new LaravelTemplateWriter("/rsc/template/LICENSE");
        LaravelTemplateWriter readmeWr = new LaravelTemplateWriter("/rsc/template/Readme.md");
        LaravelTemplateWriter composerWr = new LaravelTemplateWriter("/rsc/template/composer.json");
        LaravelTemplateWriter gitignoreWr = new LaravelTemplateWriter("/rsc/template/.gitignore");
        
        licenceWr.parse("file.php.licence");
        readmeWr.parse("file.php.readme");
        composerWr.parse("file.php.composer");
        gitignoreWr.parse("file.php.gitignore");
    }
    
    public static void writeExtjs() throws IOException{
        ExtApiWriter apiWriter = new ExtApiWriter();
        ExtContentWriter contentWriter = new ExtContentWriter();
        ExtDetailWriter dialogWriter = new ExtDetailWriter();
        
        ExtColumnWriter columnWriter = new ExtColumnWriter();
        ExtFieldWriter fieldWriter = new ExtFieldWriter();
        ExtFormWriter formWriter = new ExtFormWriter();
        ExtButtonWriter buttonWriter = new ExtButtonWriter();
        ExtContentViewWriter contentViewWriter = new ExtContentViewWriter();

        apiWriter.write();
        contentWriter.write();
        dialogWriter.write();
        
        columnWriter.write();
        fieldWriter.write();
        formWriter.write();
        buttonWriter.write();
        
        contentViewWriter.write();
    }
    
    // ----------------------------------------------------

    public HashMap<String, String> getMap() {
        return values;
    }
    
    public void put(String key, String value){
        values.put(key, value);
    }
    
    public void put(String key, int value){
        values.put(key, Integer.toString(value));
    }
    
    public void put(String key, boolean value){
        values.put(key, Boolean.toString(value));
    }
    
    public String getString(String key){
        return value(key);
    }
    
    public String getString(String key, String defaultVal){
        String val = value(key);
        if(val!=null){
            return val;
        }
        return defaultVal;
    }
    
    public int getInt(String key, int defaultVal){
        try{
            return Integer.valueOf(value(key));
        }catch(NumberFormatException e){
            return defaultVal;
        }
    }
    
    public int getInt(String key){
        return Integer.parseInt(value(key));
    }
    
    public boolean getBoolean(String key, boolean defaultVal){
        try{
            return Boolean.valueOf(value(key));
        }catch(Exception e){
            return defaultVal;
        }
    }
    
    public boolean getBoolean(String key){
        return Boolean.valueOf(value(key));
    }
    
    public String value(String key){
        String val = values.get(key);
        if(val!=null){
            return val;
        }
        return getDefault(key);
    }
    
    public static String getDefault(String key){
        String val = STATIC_CONF.values.get(key);
        if(val!=null){
            return val;
        }
        return DEFAULT_CONF.values.get(key);
    }
    
    public static Config loadConfig(String filePath){
        InputStream inputstream;
        Config conf = new Config();
        try {
            inputstream = new FileInputStream(filePath);
            JsonReader reader = new JsonReader(new InputStreamReader(inputstream, "UTF-8"));
            HashMap<String,String> map = new Gson().fromJson(reader, HashMap.class);
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> pair = (Map.Entry)it.next();
                conf.put(pair.getKey(), pair.getValue());
            }
        } catch (Exception ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conf;
    }
    
    public static boolean hasStatic(){
        return STATIC_CONF!=null;
    }
    
    public static void loadStatic(String filePath ){
        STATIC_CONF = loadConfig(filePath);
    }
    
    
    public String fileName(String key){
        return value(key); 
    }
}
