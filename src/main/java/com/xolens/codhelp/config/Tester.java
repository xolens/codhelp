/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.config;


import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class Tester {
    static{
        Config.loadStatic("./source/config.json");
    }
    
    public static void main(String[] args) throws IOException {
        
        Config.loadModel("source/class.json");
        Config.loadEnum("source/enum.json");
        
        Config.writePhpPackageFile();
        Config.writePhpTemplates();
        Config.writeMigrations();
        Config.writeModels();
        Config.writeRepositories();
        Config.writeTests();
        Config.writeFactories();
        Config.writeAppApiFile();
        //*/
        
        Config.writeExtjs();
        
    }
}
