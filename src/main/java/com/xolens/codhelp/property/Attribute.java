/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.property;

/**
 *
 * @author cedrick
 */
public class Attribute extends Property {
    public String type;
    public boolean required;
    public String visibility;
    public String aggregation;
    public String defaultValue;
    public boolean isReadOnly;
    public boolean isStatic;
    public boolean isUnique;

    public Attribute() {
    }

    public String type() {
        return type;
    }

    public void type(String type) {
        this.type = type;
    }

    public boolean required() {
        return required;
    }

    public void required(boolean required) {
        this.required = required;
    }

    public String indexName() {
        return name.replaceFirst("Id$", "");
    }

    public String visibility() {
        return visibility;
    }

    public void visibility(String visibility) {
        this.visibility = visibility;
    }

    public String aggregation() {
        return aggregation;
    }

    public void aggregation(String aggregation) {
        this.aggregation = aggregation;
    }

    public String defaultValue() {
        return defaultValue;
    }

    public void defaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isReadOnly() {
        return isReadOnly;
    }

    public void readOnly(boolean isReadOnly) {
        this.isReadOnly = isReadOnly;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void isStatic(boolean isStatic) {
        this.isStatic = isStatic;
    }

    public boolean isUnique() {
        return isUnique||hasTag("unique");
    }
   
    public void unique(boolean isUnique) {
        this.isUnique = isUnique;
    }
   
    public boolean isId() {
        return name.replaceAll(" ", "").toLowerCase().equals("id");
    }
   
    public boolean isIndex() {
        return snakecased(name).endsWith("_id");
    }
   
    public boolean isIntegerType() {
        String attrtype = tag("type", type()).toLowerCase();
        return type().toLowerCase().equals("int")||type().toLowerCase().equals("integer")||attrtype.equals("int")||attrtype.equals("integer");
    }
   
    public boolean isStringType() {
        String attrtype = tag("type", type()).toLowerCase();
        return type().toLowerCase().equals("string")||attrtype.equals("string")||attrtype.equals("text");
    }
   
    public boolean isTextType() {
        String attrtype = tag("type", type()).toLowerCase();
        return type().toLowerCase().equals("text")||attrtype.equals("text");
    }
   
    public boolean isComboType() {
        String attrtype = tag("type", type()).toLowerCase();
        return type().toLowerCase().equals("bool")||type().toLowerCase().equals("boolean")||attrtype.equals("combo")||type().toLowerCase().equals("enum");
    }
   
    public boolean isDateType() {
        String attrtype = tag("type", type()).toLowerCase();
        return attrtype.equals("date");
    }
   
    public boolean isEmailType() {
        String attrtype = tag("type", type()).toLowerCase();
        return attrtype.equals("email");
    }
   
    public boolean isPhoneType() {
        String attrtype = tag("type", type()).toLowerCase();
        return attrtype.equals("phone");
    }
   
    public boolean isYearType() {
        String attrtype = tag("type", type()).toLowerCase();
        return attrtype.equals("year");
    }
}
