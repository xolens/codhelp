/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.property;

import java.util.ArrayList;

/**
 *
 * @author cedrick
 */
public class Entity extends Property{
    public String stereotype;
    public String visibility;
    public boolean isAbstract;
    public ArrayList<Attribute> attributes = new ArrayList<>();
    public ArrayList<Operation> operations = new ArrayList<>();

    public Entity() {
    }

     public boolean hasAssoc(Entity e) {
        for(Attribute a: e.attributes()){
            if(a.isIndex()&&ucFirst(a.indexName()).equals(name())){
                return true;
            }
        }
        return false;
    }

     public boolean hasRef(Entity e) {
        for(Attribute a: this.attributes()){
            if(a.isIndex()&&ucFirst(a.indexName()).equals(e.name())){
                return true;
            }
        }
        return false;
    }

    public String stereotype() {
        return stereotype;
    }

    public void stereotype(String stereotype) {
        this.stereotype = stereotype;
    }

    public String visibility() {
        return visibility;
    }
    
    public ArrayList<Attribute> attributes() {
        return attributes;
    }
    
    public ArrayList<Operation> operations() {
        return operations;
    }

    public void visibility(String visibility) {
        this.visibility = visibility;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public void isAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }
    
    public int priority(){
        int position = 0;
        try{
            position = Integer.parseUnsignedInt(tag("priority", "0"));
        }catch(Exception e){}
        return position;
    }
    
    @Override
    public String toString() {
        return GSON.toJson(this);
    }
    
    
    public boolean hasTest() {
        return !(tag("test")!=null&&tag("test").toLowerCase().equals("false"));
    }

    
}
