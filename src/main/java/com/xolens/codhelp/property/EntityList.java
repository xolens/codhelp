/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.property;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author cedrick
 */
public class EntityList {
    public static final Gson GSON = new Gson();
    public final HashMap<String, Entity> list = new HashMap<>();

    public EntityList() {}
    public EntityList(List<Entity> l) {
        load(l);
    }
    public EntityList(Entity[] l) {
        load(l);
    }

    public final void load(List<Entity> l) {
        if(l!=null){
            list.clear();
            l.forEach((e) -> {
                list.put(e.name, e);
            });
        }
    }
    
    public final void load(Entity[] l) {
        if(l!=null){
            list.clear();
            for(Entity e: l){
                list.put(e.name, e);
            }
        }
    }
    public ArrayList<Entity> values() {
        ArrayList<Entity> vals = new ArrayList<Entity>(list.values());
        vals.sort(new Comparator<Entity>() {
            @Override
            public int compare(Entity t, Entity t1) {
                return Integer.compare(t.priority(), t1.priority());
            }
        });
        return vals;
    }

    public ArrayList<Entity> associated(Entity entity) {
        ArrayList<Entity> vals = new ArrayList<>();
        for(Entity e: values()){
            if(entity.hasAssoc(e)){
                vals.add(e);
            }
        }
        return vals;
    }

    public ArrayList<Entity> referenced(Entity entity) {
        ArrayList<Entity> vals = new ArrayList<>();
        for(Entity e: values()){
            if(entity.hasRef(e)){
                vals.add(e);
            }
        }
        return vals;
    }
    
    public void put(String key, Entity e) {
        list.put(key, e);
    }
    
    public Entity get(String key) {
        return list.get(key);
    }
    
    public ArrayList<String> keys() {
        ArrayList<Entity> values = values();
        ArrayList<String> list = new ArrayList<String>();
        for(Entity e: values){
            list.add(e.name());
        }
        return list;
    }
    
    
    @Override
    public String toString() {
        return GSON.toJson(this);
    }
}
