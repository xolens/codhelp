/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.property;

import java.util.ArrayList;

/**
 *
 * @author cedrick
 */
public class Enumeration extends Property{
    public String visibility;
    public ArrayList<String> literals = new ArrayList<>();

    public Enumeration() {
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public ArrayList<String> getLiterals() {
        return literals;
    }

    public void setLiterals(ArrayList<String> literals) {
        this.literals = literals;
    }
     
    @Override
    public String toString() {
        return GSON.toJson(this);
    }
    
    public String literals(){
        String vals = "[";
        if(literals.size()>0){
            vals+= "'"+literals.get(0)+"'";
            for(int i=1;i<literals.size();i++){
                vals+= ", '"+literals.get(i)+"'";
            }
        }
        vals+="]";
        return vals;
    }
}
