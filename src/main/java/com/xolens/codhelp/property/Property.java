/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.property;

import com.google.common.base.CaseFormat;
import com.google.gson.Gson;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author cedrick
 */
public class Property {
    public static final Gson GSON = new Gson();
    public String tags;
    public String name;

    public String tags() {
        return tags;
    }

    public void tags(String tags) {
        this.tags = tags;
    }
    
    public String tag(String tag, String val) {
        if(hasTag(tag)){
            String regex = ".*"+tag+"\\.([^\\s]*).*";
            Pattern p = Pattern.compile(regex);
            Matcher m =  p.matcher(tags);
            if(m.matches()){
                return m.group(1);
            }
        }
        return val;
    }
    
    public String tag(String tag) {
        return tag(tag,"");
    }
    
    public boolean hasTag(String tag) {
        return tags!=null&&tags.toLowerCase().contains(tag.toLowerCase());
    }
    
    
    public String name() {
        return name;
    }

    public void name(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        return GSON.toJson(this);
    }
    public static String foreignToName(String s){
        String result = null;
        if(s!=null){
            result = camelcased(s.replaceAll("_id$",""));
        }
        return result;
    }
    
    public static String uForeignToName(String s){
        String result = null;
        if(s!=null){
            result = uCamelcased(s.replaceAll("_id$",""));
        }
        return result;
    }
    
    public static String uCamelcased(String s){
        String result = null;
        if(s!=null){
            result = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, s);
        }
        return result;
    }
    
    public static String camelcased(String s){
        String result = null;
        if(s!=null){
            result = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, s);
        }
        return result;
    }
    
    public static String snakecased(String s){
        String result = null;
        if(s!=null){
            result = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, s);
        }
        return result;
    }
    
    public static String quoted(String s){
        String result = null;
        if(s!=null){
            
        }
        return result;
    }
    
    public static String ucFirst(String s){
        String result = null;
        if(s!=null&&s.length()>0){
            String firstChar = s.substring(0,1);
            result = s.replaceFirst(firstChar, firstChar.toUpperCase());
        }
        return result;
    }
    
    public static String ulFirst(String s){
        String result = null;
        if(s!=null&&s.length()>0){
            String firstChar = s.substring(0,1);
            result = s.replaceFirst(firstChar, firstChar.toLowerCase());
        }
        return result;
    }
}
