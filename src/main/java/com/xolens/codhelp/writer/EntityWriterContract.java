/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer;

import com.xolens.codhelp.property.Entity;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public interface EntityWriterContract  {
    
    public Entity entity();
    
    public String className();
    
    public String fileName();
    
    public BufferedWriter fileWriter() throws IOException;
    
    public void write() throws IOException;

}
