/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer;

import com.xolens.codhelp.config.Config;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cedrick
 */
public abstract class Writer extends Config {
    
    public InputStream getRscInputStream(String rscPath){
        InputStream inputstream = null;
        try {
            inputstream = getClass().getResourceAsStream(rscPath);            
        } catch (Exception ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inputstream;
    }
    
    public BufferedReader getRscBufferedReader(String rscPath) throws FileNotFoundException, UnsupportedEncodingException{
        BufferedReader br = new BufferedReader(new InputStreamReader(getRscInputStream(rscPath), "UTF-8"));
        return br;
    }
    
    public BufferedReader getBufferedReader(String filePath) throws FileNotFoundException{
        File file = new File(filePath);
        BufferedReader br = new BufferedReader(new FileReader(file));
        return br;
    }
    
    public BufferedWriter getBufferedWriter(String filePath) throws IOException{
        File file = new File(filePath);
        file.getParentFile().mkdirs();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        return bw;
    }    
    
    
    abstract public String outputDir();
    
    public String filePath(String fPath){
        String path = outputDir();
        if(!path.endsWith(pathSeparator())){
            path+=pathSeparator() + fPath;
        }
        return path;
    }
    
    public String dirPath(String...paths){
        String path = paths[0];
        if(!path.endsWith(pathSeparator())){
            path+=pathSeparator();
        }
        for(int i=1; i<paths.length; i++){
            path+=paths[i]+pathSeparator();
        }
        return path;
    }
    
    public String filePath(String fileName, String...paths){
        return dirPath(paths)+fileName;
    }
    
    public String pathSeparator(){
        return File.separator;
    }
    
    
    public String packageName(){
        return super.value("package.laravel.base");
    }
    
    public String packageAuthor(){
        return super.value("package.author.base");
    }
}
