/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs;

import com.xolens.codhelp.property.Entity;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class ExtApiWriter extends ExtjsRscWriter{

    @Override
    public String className() {
        return "Api";
    }

    @Override
    public void write() throws IOException {
        BufferedWriter wr = fileWriter();
        wr.write("Ext.define('"+namespace()+"', {");wr.newLine();
        wr.write("    singleton: true,");wr.newLine();wr.newLine();
        for(Entity e: list().values()){
            String loweName = e.name().toLowerCase();
            String baseUrl = "api/"+packageAuthor().toLowerCase()+"/"+packageName(true).toLowerCase()+"/"+loweName;
            wr.write("    "+loweName+": {");wr.newLine();
            wr.write("        storeUrl: '"+baseUrl+"/index',");wr.newLine();
            wr.write("        singleUrl: '"+baseUrl+"/single',");wr.newLine();
            wr.write("        deleteUrl: '"+baseUrl+"/delete',");wr.newLine();
            for(Entity assocEntity: list().associated(e)){
                wr.write("        "+assocEntity.name().toLowerCase()+": {");wr.newLine();
                wr.write("            storeUrl: '"+baseUrl+"/{id}/"+assocEntity.name().toLowerCase()+"/index',");wr.newLine();
                wr.write("            singleUrl: '"+baseUrl+"/{id}/"+assocEntity.name().toLowerCase()+"/single',");wr.newLine();
                wr.write("            deleteUrl: '"+baseUrl+"/{id}/"+assocEntity.name().toLowerCase()+"/delete',");wr.newLine();
                wr.write("        },");wr.newLine();
            }
            wr.write("    },");wr.newLine();
        }
        wr.write("})");wr.newLine();
        wr.write("");wr.newLine();
        wr.close();
    }
}
