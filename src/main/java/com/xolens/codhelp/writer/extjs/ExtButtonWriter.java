/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs;

import com.xolens.codhelp.property.Entity;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class ExtButtonWriter extends ExtjsRscWriter{

    @Override
    public String className() {
        return "Button";
    }

    @Override
    public void write() throws IOException {
        BufferedWriter wr = fileWriter();
        wr.write("Ext.define('"+namespace()+"', {");wr.newLine();
        wr.write("    singleton: true,");wr.newLine();
        wr.write("})");wr.newLine();
        wr.write("");wr.newLine();
        wr.close();
    }    
}
