/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs;

import com.xolens.codhelp.property.Entity;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class ExtContentViewWriter extends ExtjsWriter{

    @Override
    public String className() {
        return null;
    }

    @Override
    public String namespace(String className){
        return packageAuthor()+".module."+packageName()+".rsc."+className;
    }
    
    @Override
    public void write() throws IOException {
        writeContentView();
        writeContentViewController();
    }

    public void writeContentView() throws IOException {
        BufferedWriter wr = fileWriter("ContentView");
        wr.write("Ext.define('"+super.namespace("ContentView")+"', {");wr.newLine();
       
        wr.write("    extend: 'Xolens.view.ContentView',");wr.newLine();
        wr.write("    controller: '"+packageName().toLowerCase()+"-contentview',");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    context:{");wr.newLine();
        wr.write("        Api: "+namespace("Api")+",");wr.newLine();
        wr.write("        Button: "+namespace("Button")+",");wr.newLine();
        wr.write("        Column: "+namespace("Column")+",");wr.newLine();
        wr.write("        Content: "+namespace("Content")+",");wr.newLine();
        wr.write("        Dialog: "+namespace("Dialog")+",");wr.newLine();
        wr.write("        Field: "+namespace("Field")+",");wr.newLine();
        wr.write("        Form: "+namespace("Form")+",");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("});");wr.newLine();
        
        wr.close();
    }

    public void writeContentViewController() throws IOException {
        BufferedWriter wr = fileWriter("ContentViewController");
        wr.write("Ext.define('"+super.namespace("ContentViewController")+"', {");wr.newLine();
        wr.write("    extend: 'Xolens.view.ContentViewController',");wr.newLine();
        wr.write("    alias: 'controller."+packageName().toLowerCase()+"-contentview',");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("});");wr.newLine();
        wr.close();
    }
}
