/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs;

import com.xolens.codhelp.property.Entity;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class ExtContentWriter extends ExtjsRscWriter{

    @Override
    public String className() {
        return "Content";
    }

    @Override
    public void write() throws IOException {
        BufferedWriter wr = fileWriter();
        wr.write("Ext.define('"+namespace()+"', {");wr.newLine();
        wr.write("    singleton: true,");wr.newLine();
        wr.write("    require:[");wr.newLine();
        wr.write("        '"+namespace("Api")+"',");wr.newLine();
        wr.write("        '"+namespace("Form")+"',");wr.newLine();
        wr.write("        '"+namespace("Detail")+"',");wr.newLine();
        wr.write("    ],");wr.newLine();wr.newLine();
        for(Entity e: list().values()){
                String loweName = e.name().toLowerCase();
                String upperName = e.name().toUpperCase();
                
                wr.write("    "+loweName+": {");wr.newLine();
                wr.write("        paging: true,");wr.newLine();
                wr.write("        searchtool: true,");wr.newLine();
                wr.write("        title: \"Gestion des "+loweName+"s\",");wr.newLine();
                wr.write("        subtitle: \""+upperName+"S\",");wr.newLine();
                wr.write("        iconCls: 'x-fa fa-send',");wr.newLine();
                wr.write("        storeUrl: "+namespace("Api")+"."+loweName+".storeUrl,");wr.newLine();
                wr.write("        deleteUrl: "+namespace("Api")+"."+loweName+".deleteUrl,");wr.newLine();
                wr.write("        form: "+namespace("Form")+"."+loweName+",");wr.newLine();
                wr.write("        detail: "+namespace("Detail")+"."+loweName+",");wr.newLine();
                wr.write("        columns: ["+columnsString(e)+" 'editAction', 'deleteAction'],");wr.newLine();
                wr.write("        buttons: ['deletebtn', 'updatebtn', 'exportmenu', 'addbtn', 'plusmenu'],");wr.newLine();
                wr.write("        dialogs: [ ],");wr.newLine();
                wr.write("        extraConfig: {}");wr.newLine();
                wr.write("    },");wr.newLine();
        }
        wr.write("})");wr.newLine();
        wr.write("");wr.newLine();
        wr.close();
    }    
}
