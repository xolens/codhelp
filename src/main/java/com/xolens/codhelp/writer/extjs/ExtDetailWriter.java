/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs;

import static com.xolens.codhelp.config.Config.list;
import com.xolens.codhelp.property.Entity;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class ExtDetailWriter extends ExtjsRscWriter{

    @Override
    public String className() {
        return "Detail";
    }

    @Override
    public void write() throws IOException {
        BufferedWriter wr = fileWriter();
        wr.write("Ext.define('"+namespace()+"', {");wr.newLine();
        wr.write("    singleton: true,");wr.newLine();wr.newLine();
        for(Entity e: list().values()){
            if(!list().associated(e).isEmpty()){
                String loweName = e.name().toLowerCase();
                String upperName = e.name().toUpperCase();
                wr.write("");wr.newLine(); 
                wr.write("    "+loweName+": {");wr.newLine();
                wr.write("        subtitle: \""+upperName+"\",");wr.newLine();
                wr.write("        dataProperties: { "+dataProperties(e)+"},");wr.newLine();
                wr.write("        tabitems: [");
                for(Entity assocEntity: list().associated(e)){
                    wr.write("{");wr.newLine();
                    wr.write("            viewtype: '"+assocEntity.name().toLowerCase()+"',");wr.newLine();
                    wr.write("            config: { title: '"+Entity.ucFirst(assocEntity.name())+"'},");wr.newLine();
                    wr.write("            overrides: {");wr.newLine();
                    wr.write("                content: {");wr.newLine();
                    wr.write("                    storeUrl: "+namespace("Api")+"."+loweName+"."+assocEntity.name().toLowerCase()+".storeUrl,");wr.newLine();
                    wr.write("                    deleteUrl: "+namespace("Api")+"."+loweName+"."+assocEntity.name().toLowerCase()+".deleteUrl,");wr.newLine();
                    wr.write("                },");wr.newLine();
                    wr.write("                form: {");wr.newLine();
                    wr.write("                    createUrl: "+namespace("Api")+"."+loweName+"."+assocEntity.name().toLowerCase()+".storeUrl,");wr.newLine();
                    wr.write("                    updateUrl: "+namespace("Api")+"."+loweName+"."+assocEntity.name().toLowerCase()+".singleUrl,");wr.newLine();
                    wr.write("                    fieldTypes: ["+dependenceFieldsString(assocEntity, e.name(), namespace())+" ]");wr.newLine();
                    wr.write("                }");wr.newLine();
                    wr.write("            },");wr.newLine();
                    wr.write("        },");
                }
                wr.write("]");wr.newLine();
                wr.write("    },");wr.newLine();
            }
        }
        wr.write("})");wr.newLine();
        wr.write("");wr.newLine();
        wr.close();
    }
}
