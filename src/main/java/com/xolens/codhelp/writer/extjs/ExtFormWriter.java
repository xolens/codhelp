/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author cedrick
 */
public class ExtFormWriter extends ExtjsRscWriter{

    @Override
    public String className() {
        return "Form";
    }

    @Override
    public void write() throws IOException {
        BufferedWriter wr = fileWriter();
        wr.write("Ext.define('"+namespace()+"', {");wr.newLine();
        wr.write("    singleton: true,");wr.newLine();wr.newLine();
        for(Entity e: list().values()){
            String loweName = e.name().toLowerCase();
            String upperName = e.name().toUpperCase();
                
            wr.write("        "+loweName+": {");wr.newLine();
            wr.write("            subtitle: '"+upperName+"S',");wr.newLine();
            wr.write("            iconCls: 'x-fa fa-send',");wr.newLine();
            wr.write("            prefilled: [],");wr.newLine();
            wr.write("            fillables: ["+fillableString(e)+"],");wr.newLine();
            wr.write("            createUrl: "+namespace("Api")+"."+loweName+".storeUrl,");wr.newLine();
            wr.write("            updateUrl: "+namespace("Api")+"."+loweName+".singleUrl,");wr.newLine();
            wr.write("            fieldTypes: ["+fieldsString(e)+"]");wr.newLine();
            wr.write("        },");wr.newLine();
        }
        wr.write("})");wr.newLine();
        wr.write("");wr.newLine();
        wr.close();
    }
    
    public String fillableString(Entity e){
        String s =  "";
        ArrayList<Attribute> attrs = e.attributes();
        if(attrs!=null){
            for(Attribute a: attrs){
                s+="'"+Entity.snakecased(a.name())+"', ";                
                if(a.isIndex()){
                    s+="'"+Entity.snakecased(a.indexName())+"', ";                
                }
            }
        }
        return s;
    }
    
    public String fieldsString(Entity e){
        String columns =  "";
        ArrayList<Attribute> attrs = e.attributes();
        if(attrs!=null){
            for(Attribute a: attrs){
                if(a.isId()){
                    columns+="'hiddentoken', ";                
                    columns+="'hiddenid', ";                
                }else if(a.isIndex()){
                    if(a.required()){
                        columns+="'"+formatAttrName(a)+"', ";               
                    }else{
                        columns+="'"+formatAttrName(a)+"', ";               
                    }
                }else{
                    columns+="'"+formatAttrName(a)+"', ";                
                }
            }
        }
        return columns;
    }
    
}
