/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs;

/**
 *
 * @author cedrick
 */
public abstract class ExtjsRscWriter extends ExtjsWriter{
    
    @Override
    public String outputDir() {
        String extjsDir = super.value("config.output.dir.extjs");
        return  dirPath(extjsDir, DIR_EXTJS, DIR_MODULE, packageName().toLowerCase(), DIR_RSC);
    }
    
    @Override
    public String namespace(){
        return packageAuthor()+".module."+packageName()+".rsc."+className();
    }
    
    @Override
    public String namespace(String s){
        return packageAuthor()+".module."+packageName()+".rsc."+s;
    }
    
}
