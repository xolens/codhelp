/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs;

import static com.xolens.codhelp.config.Config.list;
import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.Writer;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author cedrick
 */
public abstract class ExtjsWriter extends Writer{
    public static String DIR_EXTJS = "extjs";
    public static String DIR_MODULE = "module";
    public static String DIR_RSC = "rsc";
    
    abstract public String className();
    
    abstract public void write() throws IOException;
    
    public String namespace(){
        return namespace(className());
    }
    
    public String namespace(String className){
        return packageAuthor()+".module."+packageName()+"."+className;
    }
    
    public BufferedWriter fileWriter() throws IOException{
        return fileWriter(className());
    } 
    
    public BufferedWriter fileWriter(String fileName) throws IOException{
        String filePath = filePath(fileName+".js", outputDir());
        return getBufferedWriter(filePath);
    } 
    
    @Override
    public String packageName() {
        return packageName(false);
    }
    
    public String packageName(boolean hasPrefix) {
        if(hasPrefix){
            return super.packageName();
        }
        return super.packageName().replaceFirst("PgLara", "");
    }
    
    @Override
    public String outputDir() {
        String extjsDir = super.value("config.output.dir.extjs");
        return  dirPath(extjsDir, DIR_EXTJS, DIR_MODULE, packageName().toLowerCase());
    }
    
    // ------------------------------------------
    
    public String indexColumnString(Attribute attr){
        String s =  "";
        if(attr.isIndex()){
            Entity e = list().get(Entity.ucFirst(attr.indexName()));
            s =  indexColumnName(attr)+":{ text: 'Informations "+Entity.snakecased(e.name()).replaceAll("_", " ")+"' ,columns:[";
            for(Attribute a: e.attributes()){
                if(a.hasTag("embeddable")){
                    s+="{ text: '"+Entity.ucFirst(a.name())+"', dataIndex: '"+Entity.snakecased(e.name())+"_"+Entity.snakecased(a.name())+"', minWidth: 200},";
                }
            }
            s+="]},";
        }
        return s;
    }
     
    public String indexColumnName(Attribute a){
      String s =  a.indexName().toLowerCase();
      return s;
    }
      
    public static String jsonProperty(Attribute a, String tag, String val){
      String s = "";
      if(a.hasTag(tag)){
          return tag+":"+a.tag(tag, val)+", ";
      }
      return s;
    }
    
    public String columnsString (Entity entity){
        String columns =  "";
        if(entity.attributes!=null){
            for(Attribute a: entity.attributes){
                if(a.isId()){
                }else if(a.isIndex()){
                    columns+="'"+indexColumnName(a)+"', ";                    
                }else{
                    columns+="'"+Entity.camelcased(a.name()).toLowerCase()+"', ";                    
                }
            }

        }
        return columns;
    }
    
    public static String dependenceFieldsString(Entity e, String depencenceName,String namespace){
        String columns =  "";
        ArrayList<Attribute> attrs = e.attributes();
        if(attrs!=null){
            for(Attribute a: attrs){
                if(a.isId()){
                    columns+="'hiddentoken', ";                
                    columns+="'hiddenid', ";                
                }else if(a.isIndex()){
                    if(!Entity.ucFirst(a.indexName()).equals(depencenceName)){
                        if(a.required()){
                            columns+="'rselect"+namespace+Entity.camelcased(a.indexName()).toLowerCase()+"', ";      
                        }else{
                            columns+="'bselect"+Entity.camelcased(a.indexName()).toLowerCase()+"', ";               
                        }
                    }
                }else{
                    if(a.required()){
                        columns+="'r"+Entity.camelcased(a.name()).toLowerCase()+"', ";                
                    }else{
                        columns+="'b"+Entity.camelcased(a.name()).toLowerCase()+"', ";                
                    }
                }
            }
        }
        return columns;
    }
     
       
    public String dataProperties(Entity e){
        String s =  "";
        ArrayList<Attribute> attrs = e.attributes();
        if(attrs!=null){
            for(Attribute a: attrs){
                if(a.isId()){
                }else if(a.isIndex()){
                }else{
                    s+= Entity.snakecased(a.name())+": '"+Entity.ucFirst(a.name())+"', ";
                }
            }
        }
        return s;
    }
    
     public String formatAttrName(Attribute a){
        String name = a.required()?"r":"b";
        if(a.isIndex()){
            name+= "Select"+Entity.ucFirst(a.name()).replaceAll("Id$", "");
        }else{
            name+=Entity.ucFirst(a.name());
        }
        return name;
    }
     
    public static String fieldType(Attribute a){
        String fieldType = a.required()?"r":"";
        if(a.isComboType()){
            fieldType+="combofield";
        }else if(a.isYearType()){
            fieldType+="yearfield";
        }else if(a.isIntegerType()){
            fieldType+="numberfield";
        }else if(a.isTextType()){
            fieldType+="areafield";
        }else if(a.isDateType()){
            fieldType+="datefield";
        }else if(a.isEmailType()){
            fieldType+="emailfield";
        }else if(a.isPhoneType()){
            fieldType+="phonefield";
        }else if(a.isStringType()){
            fieldType+="textfield";
        }else {
            fieldType+="textfield";
        }    
        return fieldType;
    }
}
