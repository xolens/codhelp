/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs.filtered;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class ExtColumnWriter extends ExtjsFilteredWriter{

    @Override
    public String className() {
        return "Column";
    }

    @Override
    public void write() throws IOException {
        BufferedWriter wr = fileWriter();
        wr.write("Ext.define('"+namespace()+"', {");wr.newLine();
        wr.write("    singleton: true,");wr.newLine();wr.newLine();
        for(Entity e: list().values()){
            for(Attribute a: e.attributes){
                if(a.isId()){
                }else if(a.isIndex()){
                    String columnName = indexColumnName(a);
                    if(!containtKey(columnName)){
                        addKey(columnName);
                        wr.write("    "+indexColumnString(a));wr.newLine();
                    }
                }else{
                    String columnName = a.name().toLowerCase();
                    if(!containtKey(columnName)){
                        addKey(columnName);
                        wr.write("    "+columnName+": { text: '"+Entity.ucFirst(a.name())+"', dataIndex: '"+Entity.snakecased(a.indexName())+"', minWidth: 200, "+jsonProperty(a, "flex","1").replaceAll(", $", "")+" },");wr.newLine();
                    }
                }
            }
        }
        wr.write("})");wr.newLine();
        wr.write("");wr.newLine();
        wr.close();
    }    
}
