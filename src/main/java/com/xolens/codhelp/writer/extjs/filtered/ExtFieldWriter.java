/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs.filtered;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class ExtFieldWriter extends ExtjsFilteredWriter{

    @Override
    public String className() {
        return "Field";
    }

    @Override
    public void write() throws IOException {
        BufferedWriter wr = fileWriter();
        wr.write("Ext.define('"+namespace()+"', {");wr.newLine();
        wr.write("    singleton: true,");wr.newLine();wr.newLine();
        for(Entity e: list().values()){
            for(Attribute a: e.attributes){
                String attributeName = formatAttrName(a);
                if(!containtKey(attributeName)){
                    addKey(attributeName);
                     if(a.isId()){
                        wr.write("        hiddenToken: {xtype: 'hiddenfield',name: 'token'},");
                        wr.newLine();
                        wr.write("        hiddenId: {xtype: 'hiddenfield',name: 'id'},");
                        wr.newLine();
                    }else if(a.isIndex()){
                        wr.write("        "+formatAttrName(a)+": {fieldType: 'select',hiddenField: {name: '"+Entity.snakecased(a.indexName())+"',},selectField: {name: '"+Entity.snakecased(a.name())+"',targetDialog: '"+namespace().toLowerCase()+a.indexName()+"',fieldLabel: '"+Entity.ucFirst(a.name().replaceFirst("Id$", ""))+"',}},");wr.newLine();
                    }else if(a.isComboType()){
                        wr.write("        "+formatAttrName(a)+": { fieldType: '"+fieldType(a)+"', name: '"+Entity.snakecased(a.name())+"', fieldLabel: '"+Entity.ucFirst(a.name())+"' , storeUrl: "+namespace("Api")+".listUrl},");wr.newLine();
                    }else if(a.isYearType()){
                        wr.write("        "+formatAttrName(a)+": { fieldType: '"+fieldType(a)+"', name: '"+Entity.snakecased(a.name())+"', fieldLabel: '"+Entity.ucFirst(a.name())+"'},");wr.newLine();
                    }else if(a.isIntegerType()){
                        wr.write("        "+formatAttrName(a)+": { fieldType: '"+fieldType(a)+"', name: '"+Entity.snakecased(a.name())+"', fieldLabel: '"+Entity.ucFirst(a.name())+"'},");wr.newLine();
                    }else if(a.isTextType()){
                        wr.write("        "+formatAttrName(a)+": { fieldType: '"+fieldType(a)+"', name: '"+Entity.snakecased(a.name())+"', fieldLabel: '"+Entity.ucFirst(a.name())+"'},");wr.newLine();
                    }else if(a.isDateType()){
                        wr.write("        "+formatAttrName(a)+": { fieldType: '"+fieldType(a)+"', name: '"+Entity.snakecased(a.name())+"', fieldLabel: '"+Entity.ucFirst(a.name())+"'},");wr.newLine();
                    }else if(a.isEmailType()){
                        wr.write("        "+formatAttrName(a)+": { fieldType: '"+fieldType(a)+"', name: '"+Entity.snakecased(a.name())+"', fieldLabel: '"+Entity.ucFirst(a.name())+"'},");wr.newLine();
                    }else if(a.isPhoneType()){
                        wr.write("        "+formatAttrName(a)+": { fieldType: '"+fieldType(a)+"', name: '"+Entity.snakecased(a.name())+"', fieldLabel: '"+Entity.ucFirst(a.name())+"'},");wr.newLine();
                    }else {
                        wr.write("        "+formatAttrName(a)+": { fieldType: '"+fieldType(a)+"', name: '"+Entity.snakecased(a.name())+"', fieldLabel: '"+Entity.ucFirst(a.name())+"'},");wr.newLine();
                    }    
                }
            }
        }
        wr.write("})");wr.newLine();
        wr.write("");wr.newLine();
        wr.close();
    }
}
