/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.extjs.filtered;

import com.xolens.codhelp.writer.extjs.ExtjsRscWriter;
import java.util.HashSet;

/**
 *
 * @author cedrick
 */
public abstract class ExtjsFilteredWriter extends ExtjsRscWriter{
    private final HashSet<String> keys = new HashSet<>();
    
    public boolean containtKey(String key){
        return keys.contains(key);
    }
    
    public void addKey(String key){
        keys.add(key);
    }
    
    public void clear(){
        keys.clear();
    }
}
