/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel;

import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.migration.MigrationWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author cedrick
 */
public class LaravelAppApiWriter extends LaravelWriter{
    
    @Override
    public String namespace() {
        return null;
    }
    @Override
    public String outputDir(){
        return null;
    }
    
    @Override
    public String filePath(String key){
        String fileName;
        String pkg = value("package.laravel.base");
        String dir;
        switch(key){
            case "file.php.app.api.router":
                fileName = pkg+"Router.php";                
                dir = value("dir.src.namespace.app.api");
                break;
            case "file.php.app.api.controller.GetController":
                fileName = "GetController.php";                
                dir = value("dir.src.namespace.app.api.controller");
                break;
            case "file.php.app.api.controller.GetByController":
                fileName = "GetByController.php";                
                dir = value("dir.src.namespace.app.api.controller");
                break;
            case "file.php.app.api.controller.PostController":
                fileName = "PostController.php";                
                dir = value("dir.src.namespace.app.api.controller");
                break;
            case "file.php.app.api.controller.PostByController":
                fileName = "PostByController.php";                
                dir = value("dir.src.namespace.app.api.controller");
                break;
            default:
                return super.fileName(key);
        }
        return super.filePath(fileName, dir);
    }
    
    public String apiControllerNamespace(){
        return apiNamespace()+"\\Controller";
    }
    
    public String apiNamespace(){
        return packageAuthor()+"\\"+packageName()+"\\App\\Api";
    }
    
    
    
    public BufferedWriter fileWriter(String filePath) throws IOException{
        return getBufferedWriter(filePath(filePath));
    }
    
    public void writeRoutes() throws IOException {
        BufferedWriter wr = fileWriter("file.php.app.api.router");
        
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+apiNamespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Illuminate\\Routing\\Controller;");wr.newLine();
        wr.write("use Illuminate\\Http\\Request;");wr.newLine();
        wr.write("use Route;");wr.newLine();
        wr.write("");wr.newLine();
        
        wr.write("class "+packageName()+"Router");wr.newLine();
        wr.write("{  ");wr.newLine();
        wr.write("    public static function get(){");wr.newLine();
        wr.write("        return function(){");wr.newLine();
        wr.write("            Route::get('{subroute}/index','\\"+apiControllerNamespace()+"\\GetController@paginate');");wr.newLine();
        wr.write("            Route::get('{subroute}/single/{id}','\\"+apiControllerNamespace()+"\\GetController@get');");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("            Route::post('{subroute}/index','\\"+apiControllerNamespace()+"\\PostController@create');");wr.newLine();
        wr.write("            Route::post('{subroute}/single','\\"+apiControllerNamespace()+"\\PostController@update');");wr.newLine();
        wr.write("            Route::post('{subroute}/delete','\\"+apiControllerNamespace()+"\\PostController@delete');");wr.newLine();
        wr.write("");wr.newLine();
        for(Entity e : list().values()){
            ArrayList<Entity> associated = list().associated(e);
            if(!associated.isEmpty()){
                wr.write("            Route::get('"+e.name().toLowerCase()+"/{id}/{subroute}/index','\\"+apiControllerNamespace()+"\\GetByController@"+e.name().toLowerCase()+"By');");wr.newLine();
            }
        }
        wr.write("");wr.newLine();
        wr.write("            Route::post('{baseroure}/{id}/{subroute}/index', '\\"+apiControllerNamespace()+"\\PostByController@createBy');");wr.newLine();
        wr.write("            Route::post('{baseroure}/{id}/{subroute}/single', '\\"+apiControllerNamespace()+"\\PostByController@updateBy');");wr.newLine();
        wr.write("        };");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");wr.newLine();

        wr.close();
    }
    
    public void writeGetController() throws IOException {
        BufferedWriter wr = fileWriter("file.php.app.api.controller.GetController");

        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+apiControllerNamespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Illuminate\\Http\\Request;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Api\\Controller\\BaseController;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("");wr.newLine();
        for(Entity e : list().values()){
            wr.write("use "+LaravelEntityWriter.viewRepositoryNamespase(e)+";");wr.newLine();
        }
        wr.write("");wr.newLine();
        wr.write("class GetController extends BaseController");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    protected static $map;");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    public static function map(){");wr.newLine();
        wr.write("        if(self::$map==null){");wr.newLine();
        wr.write("            self::$map =[");wr.newLine();
        for(Entity e : list().values()){
            wr.write("                '"+e.name().toLowerCase()+"' => ['repository' => new "+LaravelEntityWriter.viewRepositoryClassName(e)+"(),'ACTION' => ['PAGINATE','GET'] ],");wr.newLine();
        }
        wr.write("            ];");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        return self::$map;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function paginate(Request $request, $subroute){");wr.newLine();
        wr.write("        if($this->has($subroute, self::ACTION_PAGINATE)){");wr.newLine();
        wr.write("            $page = $request->input('page');");wr.newLine();
        wr.write("            $limit = $request->input('limit');");wr.newLine();
        wr.write("            $sorter = self::inflateSorter($request);");wr.newLine();
        wr.write("            $filterer = self::inflateFilterer($request);");wr.newLine();
        wr.write("            return self::jsonResponse($this->repo($subroute)->paginateSortedFiltered($sorter, $filterer, $limit, $page));");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        return $this->notFound($subroute);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    protected function has($subroute, $action) {");wr.newLine();
        wr.write("        return self::hasAction(self::map(), $subroute, $action);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function repo($subroute){");wr.newLine();
        wr.write("        return self::repository(self::map(), $subroute);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");wr.newLine();
        
        wr.close();
    }
    
    public void writeGetByController() throws IOException {
        BufferedWriter wr = fileWriter("file.php.app.api.controller.GetByController");
        
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+apiControllerNamespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Illuminate\\Http\\Request;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Api\\Controller\\BaseController;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("");wr.newLine();
        for(Entity e : list().values()){
            wr.write("use "+LaravelEntityWriter.viewRepositoryNamespase(e)+";");wr.newLine();
        }
        wr.write("");wr.newLine();
        wr.write("class GetByController extends BaseController");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    protected static $map;");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    public static function map(){");wr.newLine();
        wr.write("        if(self::$map==null){");wr.newLine();
        wr.write("            self::$map =[");wr.newLine();
        for(Entity e : list().values()){
            wr.write("                '"+e.name().toLowerCase()+"' => ['repository' => new "+LaravelEntityWriter.viewRepositoryClassName(e)+"(),'ACTION' => ['PAGINATE','GET'] ],");wr.newLine();
        }
        wr.write("            ];");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        return self::$map;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        for(Entity e : list().values()){
            ArrayList<Entity> associated = list().associated(e);
            if(!associated.isEmpty()){
                wr.write("    public function "+e.name().toLowerCase()+"By(Request $request, $id, $subroute){");wr.newLine();
                wr.write("        $id = (int)$id;");wr.newLine();
                wr.write("        if($this->has($subroute, self::ACTION_PAGINATE)){");wr.newLine();
                wr.write("            $page = $request->input('page');");wr.newLine();
                wr.write("            $limit = $request->input('limit');");wr.newLine();
                wr.write("            $sorter = self::inflateSorter($request);");wr.newLine();
                wr.write("            $filterer = self::inflateFilterer($request);");wr.newLine();
                wr.write("            return self::jsonResponse($this->repo($subroute)->paginateBy"+e.name()+"SortedFiltered($id, $sorter, $filterer, $limit, $page));");wr.newLine();
                wr.write("        }");wr.newLine();
                wr.write("        return $this->notFound($subroute);");wr.newLine();
                wr.write("    }");wr.newLine();
                wr.write("");wr.newLine();
            }
        }
        wr.write("    protected function has($subroute, $action) {");wr.newLine();
        wr.write("        return self::hasAction(self::map(), $subroute, $action);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function repo($subroute){");wr.newLine();
        wr.write("        return self::repository(self::map(), $subroute);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");wr.newLine();
        wr.close();
        
        wr.close();
    }
      
    public void writePostController() throws IOException {
        BufferedWriter wr = fileWriter("file.php.app.api.controller.PostController");
                
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+apiControllerNamespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Illuminate\\Http\\Request;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Api\\Controller\\BaseController;");wr.newLine();
        wr.write("use Validator;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("");wr.newLine();
        for(Entity e : list().values()){
            wr.write("use "+LaravelEntityWriter.repositoryNamespase(e)+";");wr.newLine();
        }
        wr.write("");wr.newLine();
        wr.write("class PostController extends BaseController");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    protected static $map;");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    public static function map(){");wr.newLine();
        wr.write("        if(self::$map==null){");wr.newLine();
        wr.write("            self::$map =[");wr.newLine();
        for(Entity e : list().values()){
            wr.write("                '"+e.name().toLowerCase()+"' => ['repository' => new "+LaravelEntityWriter.repositoryClassName(e)+"(),'ACTION' => ['STORE','UPDATE','DELETE'] ],");wr.newLine();
        }
        wr.write("            ];");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        return self::$map;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function create(Request $request, $subroute){");wr.newLine();
        wr.write("        if($this->has($subroute, self::ACTION_STORE)){");wr.newLine();
        wr.write("            $data = self::escapeData($request->except('id'));");wr.newLine();
        wr.write("            $validator = Validator::make(");wr.newLine();
        wr.write("                $data, ");wr.newLine();
        wr.write("                $this->repo($subroute)->validationRules($data),");wr.newLine();
        wr.write("                self::validationMessages($data, $subroute)");wr.newLine();
        wr.write("            );");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("            if ($validator->fails()) {");wr.newLine();
        wr.write("                $errors = $validator->errors();");wr.newLine();
        wr.write("                return self::jsonError($errors->all());");wr.newLine();
        wr.write("            }");wr.newLine();
        wr.write("            return self::jsonResponse($this->repo($subroute)->create($data));");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        return $this->notFound($subroute);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    public function update(Request $request, $subroute){");wr.newLine();
        wr.write("        if($this->has($subroute, self::ACTION_UPDATE)){");wr.newLine();
        wr.write("            $id = $request->input('id');");wr.newLine();
        wr.write("            $data = self::escapeData($request->all());");wr.newLine();
        wr.write("            $validator = Validator::make(");wr.newLine();
        wr.write("                $data, ");wr.newLine();
        wr.write("                $this->repo($subroute)->validationRules($data),");wr.newLine();
        wr.write("                self::validationMessages($data, $subroute)");wr.newLine();
        wr.write("            );");wr.newLine();
        ;wr.newLine();
        wr.write("            if ($validator->fails()) {");wr.newLine();
        wr.write("                $errors = $validator->errors();");wr.newLine();
        wr.write("                return self::jsonError($errors->all());");wr.newLine();
        wr.write("            }");wr.newLine();
        wr.write("            return self::jsonResponse($this->repo($subroute)->update($id, $data));");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        return $this->notFound($subroute);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    public function delete(Request $request, $subroute){");wr.newLine();
        wr.write("        if($this->has($subroute, self::ACTION_UPDATE)){");wr.newLine();
        wr.write("            $identifiers = json_decode($request->input('identifiers'));");wr.newLine();
        wr.write("            return self::jsonResponse($this->repo($subroute)->delete($identifiers));");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        return $this->notFound($subroute);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    protected function has($subroute, $action) {");wr.newLine();
        wr.write("        return self::hasAction(self::map(), $subroute, $action);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function repo($subroute){");wr.newLine();
        wr.write("        return self::repository(self::map(), $subroute);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");wr.newLine();

        wr.close();
    }
    
    public void writePostByController() throws IOException {
        BufferedWriter wr = fileWriter("file.php.app.api.controller.PostByController");
        
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+apiControllerNamespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Illuminate\\Http\\Request;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("class PostByController extends PostController");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    public function createBy(Request $request, $baseroute, $id, $subroute){");wr.newLine();
        wr.write("        $keymap = $this->keyMap();");wr.newLine();
        wr.write("        if(array_key_exists($baseroute, $keymap)){");wr.newLine();
        wr.write("            $request->request->add([$keymap[$baseroute]=>$id]);");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        return parent::create($request, $subroute);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    public function updateBy(Request $request, $baseroute, $id, $subroute){");wr.newLine();
        wr.write("        $keymap = $this->keyMap();");wr.newLine();
        wr.write("        if(array_key_exists($baseroute, $keymap)){");wr.newLine();
        wr.write("            $request->request->add([$keymap[$baseroute]=>$id]);");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        return parent::update($request, $subroute);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    public function keyMap(){");wr.newLine();
        wr.write("        return [");wr.newLine();
        for(Entity e : list().values()){
            wr.write("            '"+Entity.snakecased(e.name())+"' => '"+Entity.snakecased(e.name())+"_id',");wr.newLine();
        }
        wr.write("        ];");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");wr.newLine();
       
        wr.close();
    }
    
    public void writeAll() throws IOException{
        writeRoutes();
        writePostController();
        writePostByController();
        writeGetByController();
        writeGetController();
    }
}
