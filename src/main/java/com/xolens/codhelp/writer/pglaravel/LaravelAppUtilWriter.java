/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel;

import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.migration.MigrationWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class LaravelAppUtilWriter extends LaravelWriter{
    
    @Override
    public String namespace() {
        return null;
    }
    @Override
    public String outputDir(){
        return null;
    }
    
    @Override
    public String filePath(String key){
        String fileName;
        String pkg = value("package.laravel.base");
        String dir;
        switch(key){
            case "file.php.app.util.pkgMigration":
                fileName = pkg+"Migration.php";                
                dir = value("dir.src.namespace.app.util");
                break;
            case "file.php.app.util.abstractMigration":
                fileName = "Abstract"+pkg+"Migration.php";
                dir = value("dir.src.namespace.app.util");
                break;
            case "file.php.app.serviceProvider":
                fileName = pkg+"ServiceProvider.php";
                dir = value("dir.src.namespace");
                break;
            case "file.php.config":
                fileName = value("package.author.base").toLowerCase()+"-"+pkg.toLowerCase()+".php";
                dir = value("dir.src.config");
                break;
            case "file.php.test.readOnlyTestPgBase":
                fileName = "ReadOnlyTest"+pkg+"Base.php";
                dir = value("dir.src.namespace.test");
                break;
            case "file.php.test.writableTestBase":
                fileName = "WritableTest"+pkg+"Base.php";
                dir = value("dir.src.namespace.test");
                break;
            case "file.php.test.cleanSchemaTest":
                fileName = "CleanSchemaTest.php";
                dir = value("dir.src.namespace.test");
                break;
            case "file.php.phpunit":
                fileName = "phpunit.xml";
                dir = super.value("config.output.dir.php");
                break;
            default:
                return super.fileName(key);
        }
        return super.filePath(fileName, dir);
    }
    
    public BufferedWriter fileWriter(String filePath) throws IOException{
        return getBufferedWriter(filePath(filePath));
    }    
    
    public void writeReadOnlyTestPgBase() throws IOException {
        BufferedWriter wr = fileWriter("file.php.test.readOnlyTestPgBase");
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+super.value("namespace.test")+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\Test\\TestCase;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\Test\\RepositoryTrait\\ReadOnlyRepositoryTestTrait;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("abstract class "+className("class.php.test.ReadOnlyTestBase")+" extends TestCase");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    use ReadOnlyRepositoryTestTrait;");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    protected $repo;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function repository(){");wr.newLine();
        wr.write("        return $this->repo;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    protected function getPackageProviders($app): array{");wr.newLine();
        wr.write("        return [");wr.newLine();
        wr.write("            '"+super.value("namespace.$")+"\\"+className("class.php.app.serviceProvider")+"',");wr.newLine();
        wr.write("        ];");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");wr.newLine();
        wr.close();
    }
    
    public void writeWritableTestBase() throws IOException {
        BufferedWriter wr = fileWriter("file.php.test.writableTestBase");
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+super.value("namespace.test")+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\Test\\TestCase;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\Test\\RepositoryTrait\\ReadableRepositoryTestTrait;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\Test\\RepositoryTrait\\WritableRepositoryTestTrait;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("abstract class "+className("class.php.test.writableTestBase")+" extends TestCase");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    use ReadableRepositoryTestTrait, WritableRepositoryTestTrait;");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    protected $repo;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function repository(){");wr.newLine();
        wr.write("        return $this->repo;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    protected function getPackageProviders($app): array{");wr.newLine();
        wr.write("        return [");wr.newLine();
        wr.write("            '"+super.value("namespace.$")+"\\"+className("class.php.app.serviceProvider")+"',");wr.newLine();
        wr.write("        ];");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function generateSingleItem(){");wr.newLine();
        wr.write("        return $this->generateItems(1)[0];");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    abstract public function generateItems($toGenerateCount);");wr.newLine();
        wr.write("}");wr.newLine();
        wr.close();
    }
    
    public void writeConfig() throws IOException {
        BufferedWriter wr = fileWriter("file.php.config");
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("return [");wr.newLine();
        wr.write("    '"+value("package.laravel.base").toLowerCase()+"-enable_database_log' => true,");wr.newLine();
        wr.write("    '"+value("package.laravel.base").toLowerCase()+"-database_table_prefix' => '"+value("package.laravel.base").toLowerCase()+"_',");wr.newLine();
        wr.write("    '"+value("package.laravel.base").toLowerCase()+"-database_trigger_prefix' => '"+value("package.laravel.base").toLowerCase()+"_',");wr.newLine();
        wr.write("];");
        wr.close();
    }
    
    public void writeServiceProvider() throws IOException {
        BufferedWriter wr = fileWriter("file.php.app.serviceProvider");
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+super.value("namespace.$")+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Illuminate\\Foundation\\AliasLoader;");wr.newLine();
        wr.write("use Illuminate\\Support\\ServiceProvider;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\PgLarautilServiceProvider;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("class "+className("class.php.app.serviceProvider")+" extends ServiceProvider");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    public function boot()");wr.newLine();
        wr.write("    {");wr.newLine();
        wr.write("        $this->app->register(PgLarautilServiceProvider::class);");wr.newLine();
        wr.write("        $this->app->make('Illuminate\\Database\\Eloquent\\Factory')->load(__DIR__ . '/../../database/factories');");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("        $this->publishes([");wr.newLine();
        wr.write("            __DIR__.'/../../config/"+value("package.author.base").toLowerCase()+"-"+value("package.laravel.base").toLowerCase()+".php"+"' => config_path('"+value("package.author.base").toLowerCase()+"-"+value("package.laravel.base").toLowerCase()+".php"+"'),");wr.newLine();
        wr.write("        ]);");wr.newLine();
        wr.write("        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function register()");wr.newLine();
        wr.write("    {");wr.newLine();
        wr.write("        $this->mergeConfigFrom(");wr.newLine();
        wr.write("            __DIR__.'/../../config/"+value("package.author.base").toLowerCase()+"-"+value("package.laravel.base").toLowerCase()+".php"+"', '"+value("package.author.base").toLowerCase()+"-"+value("package.laravel.base").toLowerCase()+"'");wr.newLine();
        wr.write("        );");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");
        wr.close();

        wr.close();
    }
    
    public void writeAbstractMigration() throws IOException {
        BufferedWriter wr = fileWriter("file.php.app.util.abstractMigration");
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+super.value("namespace.app.util")+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Util\\AbstractPgLarautilMigration;");wr.newLine();
        wr.write("use Illuminate\\Support\\Facades\\DB;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("abstract class "+className("class.php.app.util.abstractMigration")+" extends AbstractPgLarautilMigration {");wr.newLine();
        wr.write("}");wr.newLine();

        wr.close();
    }
    
    public void writePackageMigration() throws IOException {
        BufferedWriter wr = fileWriter("file.php.app.util.pkgMigration");
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+super.value("namespace.app.util")+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Illuminate\\Support\\Facades\\DB;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("abstract class "+className("class.php.app.util.pkgMigration")+" extends "+className("class.php.app.util.abstractMigration"));wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    public static function tablePrefix(){");wr.newLine();
        wr.write("        return config('"+value("package.author.base").toLowerCase()+"-"+value("package.laravel.base").toLowerCase()+"."+value("package.laravel.base").toLowerCase()+"-database_table_prefix');");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public static function triggerPrefix(){");wr.newLine();
        wr.write("        return config('"+value("package.author.base").toLowerCase()+"-"+value("package.laravel.base").toLowerCase()+"."+value("package.laravel.base").toLowerCase()+"-database_trigger_prefix');");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public static function logEnabled(){");wr.newLine();
        wr.write("        return config('"+value("package.author.base").toLowerCase()+"-"+value("package.laravel.base").toLowerCase()+"."+value("package.laravel.base").toLowerCase()+"-enable_database_log');");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");wr.newLine();
        wr.close();
    }
      
    public void writeCleanSchemaTest() throws IOException {
        BufferedWriter wr = fileWriter("file.php.test.cleanSchemaTest");
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+super.value("namespace.test")+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use DB;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\Test\\CleanSchemaBase;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("final class "+className("class.php.test.cleanSchemaTest")+" extends CleanSchemaBase");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    protected function getPackageProviders($app): array{");wr.newLine();
        wr.write("        return [");wr.newLine();
        wr.write("            'Xolens\\PgLarautil\\PgLarautilServiceProvider',");wr.newLine();
        wr.write("            '"+super.value("namespace.$")+"\\"+className("class.php.app.serviceProvider")+"',");wr.newLine();
        wr.write("        ];");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");wr.newLine();
        wr.close();
    }
    
    public void writePackageUnitTest() throws IOException {
        BufferedWriter wr = fileWriter("file.php.phpunit");
        wr.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");wr.newLine();
        wr.write("<phpunit xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");wr.newLine();
        wr.write("         xsi:noNamespaceSchemaLocation=\"phpunit.xsd\"");wr.newLine();
        wr.write("         bootstrap=\"vendor/autoload.php\"");wr.newLine();
        wr.write("         verbose=\"true\">");wr.newLine();
        wr.write("    <php>");wr.newLine();
        wr.write("        <env name=\"DB_CONNECTION\" value=\""+value("config.database.connection")+"\"/>");wr.newLine();
        wr.write("        <env name=\"DB_HOST\" value=\""+value("config.database.host")+"\"/>");wr.newLine();
        wr.write("        <env name=\"DB_PORT\" value=\""+value("config.database.port")+"\"/>");wr.newLine();
        wr.write("        <env name=\"DB_DATABASE\" value=\""+value("config.database.database")+"\"/>");wr.newLine();
        wr.write("        <env name=\"DB_SCHEMA\" value=\""+value("config.database.schema")+"\"/>");wr.newLine();
        wr.write("        <env name=\"DB_USERNAME\" value=\""+value("config.database.username")+"\"/>");wr.newLine();
        wr.write("        <env name=\"DB_PASSWORD\" value=\""+value("config.database.password")+"\"/>");wr.newLine();
        wr.write("    </php>");wr.newLine();
        wr.write("    <testsuites>");wr.newLine();
        wr.write("        <testsuite name=\"test-reset-migration\">");wr.newLine();
        wr.write("            <file>src/"+value("namespace.test").replace("\\", "/")+"/"+className("class.php.test.cleanSchemaTest")+".php</file>");wr.newLine();
        wr.write("        </testsuite> ");wr.newLine();
        wr.write("        <testsuite name=\"test-repository\">");wr.newLine();
        for(Entity e: list().values()){
            wr.write("            <file>src/"+MigrationWriter.repositoryTestNamespase(e).replace("\\", "/")+".php</file>");wr.newLine();
        }
        wr.write("        </testsuite>");wr.newLine();
        wr.write("        <testsuite name=\"test-view-repository\">");wr.newLine();
        for(Entity e: list().values()){
            wr.write("            <file>src/"+MigrationWriter.viewRepositoryTestNamespase(e).replace("\\", "/")+".php</file>");wr.newLine();
        }
        wr.write("        </testsuite> ");wr.newLine();
        wr.write("    </testsuites>");wr.newLine();
        wr.write("</phpunit>");
        wr.close();
    }
    
    public void writeAll() throws IOException{
        writeAbstractMigration();
        writePackageMigration();
        writeServiceProvider();
        writeConfig();
        writeReadOnlyTestPgBase();
        writeWritableTestBase();
        writeCleanSchemaTest();
        writePackageUnitTest();
    }
}
