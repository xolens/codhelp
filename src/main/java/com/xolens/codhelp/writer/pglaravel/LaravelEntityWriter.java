/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel;

import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.EntityWriterContract;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public abstract class LaravelEntityWriter extends LaravelWriter implements EntityWriterContract {

    private final Entity entity;
        
    public LaravelEntityWriter(Entity e){
        entity = e;
    }
    
    @Override
    public Entity entity() {
        return entity;
    }

    @Override
    public BufferedWriter fileWriter() throws IOException {
        return getBufferedWriter(filePath(fileName(), outputDir()));
    }
    
    @Override
    public String fileName() {
        return className()+".php";
    }

    public static String migrationClassName(Entity e){
        return MIGRATION.packageName()+"CreateTable"+e.name();
    }

    public static String migrationViewClassName(Entity e){
        return MIGRATION.packageName()+"CreateView"+e.name();
    }

    public static String modelClassName(Entity e){
        return e.name();
    }

    public static String modelViewClassName(Entity e){
        return e.name()+"View";
    }

    public static String factoryClassName(Entity e){
        return e.name()+"Factory";
    }
    
    public static String repositoryClassName(Entity e){
        return e.name()+"Repository";
    }

    public static String viewRepositoryClassName(Entity e){
        return e.name()+"ViewRepository";
    }
    
    public static String repositoryContractClassName(Entity e){
        return e.name()+"RepositoryContract";
    }

    public static String viewRepositoryContractClassName(Entity e){
        return e.name()+"ViewRepositoryContract";
    }

    public static String repositoryTestClassName(Entity e){
        return e.name()+"RepositoryTest";
    }

    public static String viewRepositoryTestClassName(Entity e){
        return e.name()+"ViewRepositoryTest";
    }
    
    public static String modelNamespase(Entity e){
        if(e!=null)
            return modelNamespase()+"\\"+modelClassName(e);
        return MIGRATION.value("namespace.app.model");
    }

    public static String modelNamespase(){
        return modelNamespase(null);
    }

    public static String modelViewNamespase(Entity e){
        if(e!=null)
            return modelViewNamespase()+"\\"+modelViewClassName(e);
        return MIGRATION.value("namespace.app.model.view");
    }

    public static String modelViewNamespase(){
        return modelViewNamespase(null);
    }

    public static String repositoryNamespase(Entity e){
        if(e!=null)
            return repositoryNamespase()+"\\"+repositoryClassName(e);
        return MIGRATION.value("namespace.app.repository");
    }

    public static String repositoryNamespase(){
        return repositoryNamespase(null);
    }
    
    public static String repositoryContractNamespase(Entity e){
        return repositoryNamespase(e);
    }

    public static String repositoryContractNamespase(){
        return repositoryNamespase();
    }
    
    public static String viewRepositoryContractNamespase(Entity e){
        return repositoryNamespase(e);
    }

    public static String viewRepositoryContractNamespase(){
        return viewRepositoryNamespase();
    }

    public static String viewRepositoryNamespase(Entity e){
        if(e!=null)
            return viewRepositoryNamespase()+"\\"+viewRepositoryClassName(e);
        return MIGRATION.value("namespace.app.repository.view");
    }

    public static String viewRepositoryNamespase(){
        return LaravelEntityWriter.viewRepositoryNamespase(null);
    }

    public static String repositoryTestNamespase(Entity e){
        if(e!=null)
            return repositoryTestNamespase()+"\\"+repositoryTestClassName(e);
        return MIGRATION.value("namespace.test.repository");
    }

    public static String repositoryTestNamespase(){
        return repositoryTestNamespase(null);
    }
    public static String viewRepositoryTestNamespase(Entity e){
        if(e!=null)
            return viewRepositoryTestNamespase()+"\\"+viewRepositoryClassName(e);
        return MIGRATION.value("namespace.test.repository.view");
    }

    public static String viewRepositoryTestNamespase(){
        return viewRepositoryTestNamespase(null);
    }
}
