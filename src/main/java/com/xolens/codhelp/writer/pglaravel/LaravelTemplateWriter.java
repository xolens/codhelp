/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author cedrick
 */
public class LaravelTemplateWriter extends LaravelWriter{
    
    private final String rscPath;

    public LaravelTemplateWriter(String rscPath) {
        this.rscPath = rscPath;
    }
    
    @Override
    public String outputDir(){
        return super.value("config.output.dir.php");
    }
    
    public void parse(String output) throws IOException{
        String path = filePath(fileName(output), outputDir());
        parseFile(getRscBufferedReader(rscPath), getBufferedWriter(path), this);
    }
    
    public static void parseFile(BufferedReader br, BufferedWriter bw, LaravelTemplateWriter conf) throws IOException{
        String line;
        
        while( ( line = br.readLine())!=null){
            Pattern p = Pattern.compile("\\{\\{((([^\\}])|(\\}[^\\}]))*)\\}\\}");
            Matcher m =  p.matcher(line);
            while(m.find()){
                String exp = "\\{\\{"+m.group(1)+"\\}\\}";
                String expVal = m.group(1).replaceAll("\\s", "");
                line = line.replaceFirst(exp, conf.value(expVal, "{{ null }}").replace("\\", "\\\\"));
            }
            bw.write(line);
            bw.newLine();
        }
         bw.close();
         br.close();
    }
    
    public String value(String key, String defaultVal){
        String val = value(key);
        if(val!=null){
            return val;
        }
        return defaultVal;
    }

    @Override
    public String namespace() {
        return null;
    }
}
