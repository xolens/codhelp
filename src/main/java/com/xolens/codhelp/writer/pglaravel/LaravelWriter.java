/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel;

import static com.xolens.codhelp.config.Config.enumeration;
import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.property.Enumeration;
import com.xolens.codhelp.writer.Writer;
import com.xolens.codhelp.writer.pglaravel.migration.MigrationWriter;

/**
 *
 * @author cedrick
 */
public abstract class LaravelWriter extends Writer{
    public static final MigrationWriter MIGRATION;
    static{
        Entity e = new Entity();
        e.name("StaticEntity");
        MIGRATION = new MigrationWriter(e);
    }
    
    public static final String DIR_SRC = "src";
    public static final String DIR_MODEL = "Model";
    public static final String DIR_REPOSITORY = "Repository";
    public static final String DIR_VIEW = "View";
    public static final String DIR_CONFIG = "config";
    public static final String DIR_DATABASE = "database";
    public static final String DIR_FACTORIES = "factories";
    public static final String DIR_MIGRATIONS = "migrations";
    public static final String PKG_APP = "App";
    public static final String PKG_UTIL = "Util";
    public static final String PKG_TEST = "Test";
    public static final String PKG_API = "Api";
    public static final String PKG_CONTROLLER = "Controller";
    

    abstract public String namespace();
    
    public String className(String key){
        String className;
        String pkg = value("package.laravel.base");
        switch(key){
            case "class.php.app.util.pkgMigration":
                className = pkg+"Migration";
                break;
            case "class.php.app.util.abstractMigration":
                className = "Abstract"+pkg+"Migration";
                break;
            case "class.php.app.serviceProvider":
                className = pkg+"ServiceProvider";
                break;
            case "class.php.test.ReadOnlyTestBase":
                className = "ReadOnlyTest"+pkg+"Base";
                break;
            case "class.php.test.writableTestBase":
                className = "WritableTest"+pkg+"Base";
                break;
            case "class.php.test.cleanSchemaTest":
                className = "CleanSchemaTest";
                break;
            default:
                return value(key);
        }
        return className;
    }
     
    public String namespace(String...names){
        String namespace = names[0];
        for(int i=1; i<names.length;i++){
            namespace+="\\"+names[i];
        }
        return namespace;
    }
     
    @Override
    public String value(String key){
        String val = null;
        String baseDir = super.value("config.output.dir.php");
        final String PKG_BASE = packageName();
        final String PKG_AUTHOR_BASE = packageAuthor();
        switch(key){
            case "dir.src.config":
                val = dirPath(baseDir, DIR_SRC, DIR_CONFIG);
                break;
            case "dir.src.database.migrations":
                val = dirPath(baseDir, DIR_SRC, DIR_DATABASE, DIR_MIGRATIONS);
                break;
            case "dir.src.database.factories":
                val = dirPath(baseDir, DIR_SRC, DIR_DATABASE, DIR_FACTORIES);
                break;
            case "dir.src.namespace":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE);
                break;
            case "dir.src.namespace.app":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_APP);
                break;
            case "dir.src.namespace.app.api":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, PKG_API);
                break;
            case "dir.src.namespace.app.api.controller":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, PKG_API, PKG_CONTROLLER);
                break;
                case "dir.src.namespace.app.model":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, DIR_MODEL);
                break;
            case "dir.src.namespace.app.model.view":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, DIR_MODEL, DIR_VIEW);
                break;
            case "dir.src.namespace.app.repository":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, DIR_REPOSITORY);
                break;
            case "dir.src.namespace.app.repository.view":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, DIR_REPOSITORY, DIR_VIEW);
                break;
            case "dir.src.namespace.app.util":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, PKG_UTIL);
                break;
            case "dir.src.namespace.test":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_TEST);
                break;
            case "dir.src.namespace.test.repository":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_TEST, DIR_REPOSITORY);
                break;
            case "dir.src.namespace.test.repository.view":
                val = dirPath(baseDir, DIR_SRC, PKG_AUTHOR_BASE, PKG_BASE, PKG_TEST, DIR_REPOSITORY, DIR_VIEW);
                break;
            case "namespace.$":
                val = namespace(PKG_AUTHOR_BASE, PKG_BASE);                
                break;
            case "namespace.app":
                val = namespace(PKG_AUTHOR_BASE, PKG_BASE, PKG_APP);                
                break;
            case "namespace.app.util":
                val = namespace(PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, PKG_UTIL);                
                break;
            case "namespace.test":
                val = namespace(PKG_AUTHOR_BASE, PKG_BASE, PKG_TEST);                
                break;
            case "namespace.app.model":
                val = namespace(PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, DIR_MODEL);                
                break;
            case "namespace.app.model.view":
                val = namespace(PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, DIR_MODEL, DIR_VIEW);                
                break;
            case "namespace.app.repository":
                val = namespace(PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, DIR_REPOSITORY);                
                break;
            case "namespace.app.repository.view":
                val = namespace(PKG_AUTHOR_BASE, PKG_BASE, PKG_APP, DIR_REPOSITORY, DIR_VIEW);                
                break;
            case "namespace.test.repository":
                val = namespace(PKG_AUTHOR_BASE, PKG_BASE, PKG_TEST, DIR_REPOSITORY);                
                break;
            case "namespace.test.repository.view":
                val = namespace(PKG_AUTHOR_BASE, PKG_BASE, PKG_TEST, DIR_REPOSITORY, DIR_VIEW);                
                break;
            default: 
                val = super.value(key);
                break;
        }
        return val;
    }
    
    @Override
    public String fileName(String key){
        String val;
        switch(key){
            case "file.php.composer":
                val = "composer.json";
                break;
            case "file.php.licence":
                val = "LICENSE";
                break;
            case "file.php.readme":
                val = "README.md";
                break;
            case "file.php.gitignore":
                val = ".gitignore";
                break;
            default:
                val = super.fileName(key);
                break;
        }
        return val;
    }
    
        
    public String enumValues(Attribute a){
        String type = a.tag("type");
        Enumeration e = enumeration(type);
        if(e!=null){
           return e.literals();
        }
        return "[]";
    }
    
}
