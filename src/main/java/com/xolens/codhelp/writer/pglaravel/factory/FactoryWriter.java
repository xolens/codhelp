/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel.factory;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.LaravelEntityWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class FactoryWriter extends LaravelEntityWriter {
        
    public FactoryWriter(Entity e){
        super(e);
    }
    
    @Override
    public String namespace() {
        return modelNamespase();
    }

    @Override
    public String outputDir() {
        return value("dir.src.database.factories");
    }    
    
    @Override
    public String className() {
        return factoryClassName(entity());
    }
    
    @Override
    public void write() throws IOException {
        if(entity()==null) return;
        
        BufferedWriter wr = fileWriter();
        
        wr.write("<?php");wr.newLine();
        wr.write("use Illuminate\\Support\\Str;");wr.newLine();
        wr.write("use Faker\\Generator as Faker;");wr.newLine();
        wr.write("use "+modelNamespase(entity())+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("$factory->define("+modelClassName(entity())+"::class, function (Faker $faker) {");wr.newLine();
        wr.write("    return [");wr.newLine();
        for(Attribute a: entity().attributes){
            if(!a.isId()){
                wr.write("        "+attributeToFakerAttr(a)+",");wr.newLine();
            }
        }
        wr.write("    ];");wr.newLine();
        wr.write("});");wr.newLine();
        
        wr.close();
    }
    
    public String attributeToFakerAttr(Attribute a){
        String val="'"+Entity.snakecased(a.name())+"' => ";
        String type = a.type().toLowerCase();
        
        switch(type){
            case "enum":
                val += "$faker->randomElement($array = "+enumValues(a)+")";
                break;
            case "int":
            case "integer":
                val += "$faker->randomNumber";
                break;
            case "timestamp":
                val += "$faker->date";
                break;
            case "boolean":
                val += "$faker->randomElement($array = [true, false])";
                break;
            case "json":
                val += "'{}'";
                break;
            default:
                val += "$faker->name";
                break;
        }
        
        return val;
    }
}
