/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel.migration;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.LaravelEntityWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class MigrationViewWriter extends LaravelEntityWriter {
        
    public MigrationViewWriter(Entity e){
        super(e);
    }
    
    @Override
    public String namespace() {
        return null;
    }

    @Override
    public String outputDir() {
        return value("dir.src.database.migrations");
    }    
    
    @Override
    public String className() {
        return migrationViewClassName(entity());
    }
    
      @Override
    public String fileName() {
        return "/0000_00_00_10000"+entity().priority()+"_"+packageName().toLowerCase()+"_create_view_"+Entity.snakecased(entity().name())+".php";
    }
    
    @Override
    public void write() throws IOException {
        if(entity()==null) return;
        
        BufferedWriter wr = fileWriter();
        
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Illuminate\\Support\\Facades\\DB;");wr.newLine();
        wr.write("use "+packageAuthor()+"\\"+packageName()+"\\App\\Util\\"+packageName()+"Migration;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("class "+className()+" extends "+packageName()+"Migration");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * Return table name");wr.newLine();
        wr.write("     *");wr.newLine();
        wr.write("     * @return string");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    public static function tableName(){");wr.newLine();
        wr.write("        return '"+Entity.snakecased(entity().name())+"_view';");wr.newLine();
        wr.write("    }    ");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * Run the migrations.");wr.newLine();
        wr.write("     *");wr.newLine();
        wr.write("     * @return void");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    public function up()");wr.newLine();
        wr.write("    {");wr.newLine();
        wr.write("        $mainTable = "+packageName()+"CreateTable"+entity().name()+"::table();");wr.newLine();
        for(Entity assocEntity: list().referenced(entity())){
            wr.write("        $"+Entity.ulFirst(assocEntity.name())+"Table = "+packageName()+"CreateTable"+assocEntity.name()+"::table();");wr.newLine();
        }
        wr.write("        DB::statement(\"");wr.newLine();
        wr.write("            CREATE VIEW \".self::table().\" AS(");wr.newLine();
        wr.write("                SELECT ");wr.newLine();
        wr.write("                    \".$mainTable.\".*");
        for(Entity assocEntity: MigrationViewWriter.list().referenced(entity())){
            String tabName = "$"+Entity.ulFirst(assocEntity.name())+"Table";
            for(Attribute a: assocEntity.attributes()){
                if(a.hasTag("embeddable")){
                    wr.write(",");wr.newLine();
                    wr.write("                    \"."+tabName+".\"."+a.name()+" as "+Entity.snakecased(assocEntity.name())+"_"+Entity.snakecased(a.name()));
                }
            }
        }
        wr.newLine();
        wr.write("                FROM \".$mainTable.\" ");wr.newLine();
        for(Entity assocEntity: list().referenced(entity())){
            String tabName = "$"+Entity.ulFirst(assocEntity.name())+"Table";
            wr.write("                    LEFT JOIN \"."+tabName+".\" ON \"."+tabName+".\".id = \".$mainTable.\"."+Entity.snakecased(assocEntity.name())+"_id");wr.newLine();
        }
        wr.write("            )");wr.newLine();
        wr.write("        \");");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * Reverse the migrations.");wr.newLine();
        wr.write("     *");wr.newLine();
        wr.write("     * @return void");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    public function down()");wr.newLine();
        wr.write("    {");wr.newLine();
        wr.write("        DB::statement(\"DROP VIEW IF EXISTS \".self::table());");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");wr.newLine();
        wr.write("");wr.newLine();
        wr.close();
    }
}
