/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel.migration;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.property.Enumeration;
import com.xolens.codhelp.writer.pglaravel.LaravelEntityWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class MigrationWriter extends LaravelEntityWriter {
        
    public MigrationWriter(Entity e){
        super(e);
    }
    
    @Override
    public String namespace() {
        return null;
    }

    @Override
    public String outputDir() {
        return value("dir.src.database.migrations");
    }    
    
    @Override
    public String className() {
        return migrationClassName(entity());
    }
    
    @Override
    public String fileName() {
        return "/0000_00_00_01000"+entity().priority()+"_"+packageName().toLowerCase()+"_create_table_"+Entity.snakecased(entity().name())+".php";
    }
    
    @Override
    public void write() throws IOException {
        if(entity()==null) return;
        
        BufferedWriter wr = fileWriter();
        
       wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Illuminate\\Support\\Facades\\Schema;");wr.newLine();
        wr.write("use Illuminate\\Database\\Schema\\Blueprint;");wr.newLine();
        wr.write("use Illuminate\\Support\\Facades\\DB;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use "+packageAuthor()+"\\"+packageName()+"\\App\\Util\\"+packageName()+"Migration;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("class "+className()+" extends "+packageName()+"Migration");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * Return table name");wr.newLine();
        wr.write("     *");wr.newLine();
        wr.write("     * @return string");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    public static function tableName(){");wr.newLine();
        wr.write("        return '"+Entity.snakecased(entity().name())+"';");wr.newLine();
        wr.write("    }    ");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * Run the migrations.");wr.newLine();
        wr.write("     *");wr.newLine();
        wr.write("     * @return void");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    public function up()");wr.newLine();
        wr.write("    {");wr.newLine();
        wr.write("        Schema::create(self::table(), function (Blueprint $table) {");wr.newLine();
        for(Attribute attr: entity().attributes){
            wr.write("            "+attributeToColumn(attr)+";");wr.newLine();            
        }
        wr.write("        });");wr.newLine();
        wr.write("        if(self::logEnabled()){");wr.newLine();
        wr.write("            self::registerForLog();");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * Reverse the migrations.");wr.newLine();
        wr.write("     *");wr.newLine();
        wr.write("     * @return void");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    public function down()");wr.newLine();
        wr.write("    {");wr.newLine();
        wr.write("        if(self::logEnabled()){");wr.newLine();
        wr.write("            self::unregisterFromLog();");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        Schema::dropIfExists(self::table());");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}");wr.newLine();

        wr.close();
    }
    public String attributeToColumn(Attribute a){
        String col = null;
        if(a!=null){
            String type = a.type().toLowerCase();
            switch(type){
                case "enum":
                    col = "$table->enum('"+Entity.snakecased(a.name())+"',"+enumValues(a)+")";
                    break;
                case "int":
                    col = "$table->integer('"+Entity.snakecased(a.name())+"')";
                    break;
                default:
                    col = "$table->"+type+"('"+Entity.snakecased(a.name())+"')";
                    break;
            }
            if(a.isUnique()){
                col+="->unique()";
            }
            if(!a.required()){
                col+="->nullable()";
            }
            if(a.hasTag("index")){
                col+="->index()";
            }
        }
        return col;
    }
    
}
