/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel.model;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.LaravelEntityWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class ModelWriter extends LaravelEntityWriter {
        
    public ModelWriter(Entity e){
        super(e);
    }
    
    @Override
    public String namespace() {
        return modelNamespase();
    }

    @Override
    public String outputDir() {
        return value("dir.src.namespace.app.model");
    }    
    
    @Override
    public String className() {
        return entity().name();
    }
    
    @Override
    public String className(String name) {
        return entity().name();
    }
    
    @Override
    public void write() throws IOException {
        if(entity()==null) return;
        
        BufferedWriter wr = fileWriter();
        
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+namespace()+";");
        wr.write("");wr.newLine();
        wr.write("use Illuminate\\Database\\Eloquent\\Model;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use "+migrationClassName(entity())+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("class "+className()+" extends Model");wr.newLine();
        wr.write("{");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("    public const "+Entity.snakecased(a.name().replaceAll("Id", "")).toUpperCase()+"_PROPERTY = '"+Entity.snakecased(a.name())+"';");wr.newLine();
            }
        }
        wr.write("");wr.newLine();
        wr.write("    public $timestamps = false;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * The attributes that are mass assignable.");wr.newLine();
        wr.write("     *");wr.newLine();
        wr.write("     * @var array");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    protected $fillable = [");wr.newLine();
        wr.write("        "+fillable(entity()));wr.newLine();
        wr.write("    ];");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * The table associated with the model.");wr.newLine();
        wr.write("     *");wr.newLine();
        wr.write("     * @var string");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    protected $table;");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    function __construct(array $attributes = []) {");wr.newLine();
        wr.write("        $this->table = "+migrationClassName(entity())+"::table();");wr.newLine();
        wr.write("        parent::__construct($attributes);");wr.newLine();
        wr.write("    }");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("");wr.newLine();
                wr.write("    public function "+a.name().replaceAll("Id", "")+"(){");wr.newLine();
                wr.write("        return $this->belongsTo('"+namespace(Entity.ucFirst(a.name().replaceAll("Id", "")))+"','"+Entity.snakecased(a.name())+"');");wr.newLine();
                wr.write("    } ");wr.newLine();           
            }
        }
        wr.write("}");wr.newLine();
        wr.close();
    }
    
    public String fillable(Entity e) {
        String fillable = "";
        for(Attribute a: e.attributes){
            fillable+="'"+Entity.snakecased(a.name())+"', ";
        }
        return fillable;
    }
    
}
