/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel.repository;

import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.LaravelEntityWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class RepositoryContractWriter extends LaravelEntityWriter {
        
    public RepositoryContractWriter(Entity e){
        super(e);
    }
    
    @Override
    public String namespace() {
        return repositoryContractNamespase();
    }

    @Override
    public String outputDir() {
        return value("dir.src.namespace.app.repository");
    }    
    
    @Override
    public String className() {
        return repositoryContractClassName(entity());
    }
    
    @Override
    public void write() throws IOException {
        if(entity()==null) return;
        
        BufferedWriter wr = fileWriter();
               
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+namespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Repository\\WritableRepositoryContract;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("interface "+className()+" extends WritableRepositoryContract");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("}");wr.newLine();
        wr.close();
    }
    
}
