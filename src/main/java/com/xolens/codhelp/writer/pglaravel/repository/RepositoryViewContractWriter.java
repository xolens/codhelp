/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel.repository;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.LaravelEntityWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class RepositoryViewContractWriter extends LaravelEntityWriter {
        
    public RepositoryViewContractWriter(Entity e){
        super(e);
    }
    
    @Override
    public String namespace() {
        return viewRepositoryNamespase();
    }

    @Override
    public String outputDir() {
        return value("dir.src.namespace.app.repository.view");
    }    
    
    @Override
    public String className() {
        return viewRepositoryContractClassName(entity());
    }
    
    @Override
    public void write() throws IOException {
        if(entity()==null) return;
        
        BufferedWriter wr = fileWriter();
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+namespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Repository\\ReadableRepositoryContract;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Util\\Model\\Filterer;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Util\\Model\\Sorter;");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("interface "+className()+" extends ReadableRepositoryContract");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("");wr.newLine();
                for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("     public function paginateBy"+Entity.ucFirst(a.name().replaceAll("Id", ""))+"($parentId, $perPage=50, $page = null,  $columns = ['*'], $pageName = 'page');");wr.newLine();
                wr.write("");wr.newLine();
                wr.write("     public function paginateBy"+Entity.ucFirst(a.name().replaceAll("Id", ""))+"Sorted($parentId, Sorter $sorter, $perPage=50, $page = null,  $columns = ['*'], $pageName = 'page');");wr.newLine();
                wr.write("");wr.newLine();
                wr.write("     public function paginateBy"+Entity.ucFirst(a.name().replaceAll("Id", ""))+"Filtered($parentId, Filterer $filterer, $perPage=50, $page = null,  $columns = ['*'], $pageName = 'page');");wr.newLine();
                wr.write("");wr.newLine();
                wr.write("     public function paginateBy"+Entity.ucFirst(a.name().replaceAll("Id", ""))+"SortedFiltered($parentId, Sorter $sorter, Filterer $filterer, $perPage=50, $page = null,  $columns = ['*'], $pageName = 'page');");wr.newLine();
                wr.write("");wr.newLine();
                
            }
        }
        wr.write("}");wr.newLine();
        wr.close();
    }
    
}
