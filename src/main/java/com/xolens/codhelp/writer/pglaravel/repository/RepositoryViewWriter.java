/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel.repository;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.LaravelEntityWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class RepositoryViewWriter extends LaravelEntityWriter {
        
    public RepositoryViewWriter(Entity e){
        super(e);
    }
    
    @Override
    public String namespace() {
        return viewRepositoryNamespase();
    }

    @Override
    public String outputDir() {
        return value("dir.src.namespace.app.repository.view");
    }    
    
    @Override
    public String className() {
        return viewRepositoryClassName(entity());
    }
    
    @Override
    public void write() throws IOException {
        if(entity()==null) return;
        
        BufferedWriter wr = fileWriter();
        
        String tempStr1 = "", tempStr2 = "";
        
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+namespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        
        wr.write("use "+modelNamespase(entity())+";");wr.newLine();
        wr.write("use "+modelViewNamespase(entity())+";");wr.newLine();
        
        wr.write("use "+viewRepositoryContractNamespase(entity())+";");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Repository\\AbstractReadableRepository;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Util\\Model\\Filterer;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Util\\Model\\Sorter;");wr.newLine();
        
        wr.write("");wr.newLine();
        wr.write("class "+className()+" extends AbstractReadableRepository implements "+viewRepositoryContractClassName(entity()));wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    public function model(){");wr.newLine();
        wr.write("        return "+modelViewClassName(entity())+"::class;");wr.newLine();
        wr.write("    }");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("     public function paginateBy"+Entity.ucFirst(a.name().replaceAll("Id", ""))+"($parentId, $perPage=50, $page = null,  $columns = ['*'], $pageName = 'page'){");wr.newLine();
                wr.write("        $parentFilterer = new Filterer();");wr.newLine();
                wr.write("        $parentFilterer->equals("+modelClassName(entity())+"::"+Entity.snakecased(a.name().replaceAll("Id", "")).toUpperCase()+"_PROPERTY, $parentId);");wr.newLine();
                wr.write("        return $this->paginateFiltered($parentFilterer, $perPage, $page,  $columns, $pageName);");wr.newLine();
                wr.write("     }");wr.newLine();
                wr.write("");wr.newLine();
                wr.write("     public function paginateBy"+Entity.ucFirst(a.name().replaceAll("Id", ""))+"Sorted($parentId, Sorter $sorter, $perPage=50, $page = null,  $columns = ['*'], $pageName = 'page'){");wr.newLine();
                wr.write("        $parentFilterer = new Filterer();");wr.newLine();
                wr.write("        $parentFilterer->equals("+modelClassName(entity())+"::"+Entity.snakecased(a.name().replaceAll("Id", "")).toUpperCase()+"_PROPERTY, $parentId);");wr.newLine();
                wr.write("        return $this->paginateSortedFiltered($sorter, $parentFilterer, $perPage, $page,  $columns, $pageName);");wr.newLine();
                wr.write("     }");wr.newLine();
                wr.write("");wr.newLine();
                wr.write("     public function paginateBy"+Entity.ucFirst(a.name().replaceAll("Id", ""))+"Filtered($parentId, Filterer $filterer, $perPage=50, $page = null,  $columns = ['*'], $pageName = 'page'){");wr.newLine();
                wr.write("        $parentFilterer = new Filterer();");wr.newLine();
                wr.write("        $parentFilterer->equals("+modelClassName(entity())+"::"+Entity.snakecased(a.name().replaceAll("Id", "")).toUpperCase()+"_PROPERTY, $parentId);");wr.newLine();
                wr.write("        $parentFilterer->and($filterer);");wr.newLine();
                wr.write("        return $this->paginateFiltered($parentFilterer, $perPage, $page,  $columns, $pageName);");wr.newLine();
                wr.write("     }");wr.newLine();
                wr.write("");wr.newLine();
                wr.write("     public function paginateBy"+Entity.ucFirst(a.name().replaceAll("Id", ""))+"SortedFiltered($parentId, Sorter $sorter, Filterer $filterer, $perPage=50, $page = null,  $columns = ['*'], $pageName = 'page'){");wr.newLine();
                wr.write("        $parentFilterer = new Filterer();");wr.newLine();
                wr.write("        $parentFilterer->equals("+modelClassName(entity())+"::"+Entity.snakecased(a.name().replaceAll("Id", "")).toUpperCase()+"_PROPERTY, $parentId);");wr.newLine();
                wr.write("        $parentFilterer->and($filterer);");wr.newLine();
                wr.write("        return $this->paginateSortedFiltered($sorter, $parentFilterer, $perPage, $page,  $columns, $pageName);");wr.newLine();
                wr.write("     }");wr.newLine();
                wr.write("");wr.newLine();
                
            }
        }
        wr.write("}");wr.newLine();
        wr.close();
    }
    
}
