/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel.repository;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.LaravelEntityWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class RepositoryWriter extends LaravelEntityWriter {
        
    public RepositoryWriter(Entity e){
        super(e);
    }
    
    @Override
    public String namespace() {
        return repositoryNamespase();
    }

    @Override
    public String outputDir() {
        return value("dir.src.namespace.app.repository");
    }    
    
    @Override
    public String className() {
        return repositoryClassName(entity());
    }
    
    @Override
    public void write() throws IOException {
        if(entity()==null) return;
        
        BufferedWriter wr = fileWriter();
        
        String tempStr1 = "", tempStr2 = "";
        
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+namespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use "+modelNamespase(entity())+";");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Repository\\AbstractWritableRepository;");wr.newLine();
        wr.write("use Illuminate\\Validation\\Rule;");wr.newLine();
        wr.write("use "+migrationClassName(entity())+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("class "+className()+" extends AbstractWritableRepository implements "+repositoryContractClassName(entity())+"");wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    public function model(){");wr.newLine();
        wr.write("        return "+modelClassName(entity())+"::class;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("    /*");wr.newLine();
        wr.write("    public function validationRules(array $data){");wr.newLine();
        wr.write("        $id = self::get($data,'id');");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.isIndex()){
                String attrName = a.name();
                String attrInput = Entity.snakecased(attrName);
                wr.write("        $"+attrName+" = self::get($data,'"+attrInput+"');");wr.newLine();
                tempStr1+=", $"+attrName;
                tempStr2+="->where('"+attrInput+"', $"+attrName+")";
            }
        }
        wr.write("        return [");wr.newLine();
        wr.write("            'id' => ['required',Rule::unique("+migrationClassName(entity())+"::table())->where(function ($query) use($id"+tempStr1+") {");wr.newLine();
        wr.write("                return $query->where('id','!=', $id)"+tempStr2+";");wr.newLine();
        wr.write("            })],");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("unique")){
                wr.write("            '"+Entity.snakecased(a.name())+"' => [Rule::unique("+migrationClassName(entity())+"::table())->where(function ($query) use($id"+tempStr1+") {");wr.newLine();
        wr.write("                return $query->where('id','!=', $id)"+tempStr2+";");wr.newLine();
        wr.write("            })],");wr.newLine();
            }
        }
        wr.write("        ];");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("    //*/");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("}");wr.newLine();

        wr.close();
    }
    
}
