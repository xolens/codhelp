/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel.test;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.LaravelEntityWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class RepositoryTestWriter extends LaravelEntityWriter {
        
    public RepositoryTestWriter(Entity e){
        super(e);
    }
    
    @Override
    public String namespace() {
        return repositoryTestNamespase();
    }

    @Override
    public String outputDir() {
        return value("dir.src.namespace.test.repository");
    }    
    
    @Override
    public String className() {
        return repositoryTestClassName(entity());
    }
    
    @Override
    public void write() throws IOException {
        if(entity()==null||!entity().hasTest()) return;
        
        BufferedWriter wr = fileWriter();
        boolean hasIndex = false;
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+namespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use "+modelNamespase(entity())+";");wr.newLine();
        wr.write("use "+repositoryNamespase(entity())+";");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("use "+repositoryNamespase(entity(Entity.ucFirst(a.name().replace("Id", ""))))+";");wr.newLine();
                hasIndex = true;
            }
        }
        wr.write("use Xolens\\PgLarautil\\App\\Util\\Model\\Sorter;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Util\\Model\\Filterer;");wr.newLine();
        wr.write("use "+value("namespace.test")+"\\"+className("class.php.test.writableTestBase")+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("final class "+className()+" extends "+className("class.php.test.writableTestBase"));wr.newLine();
        wr.write("{");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("    protected $"+a.name().replace("Id", "")+"Repo;");wr.newLine();
            }
        }
        wr.write("    /**");wr.newLine();
        wr.write("     * Setup the test environment.");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    protected function setUp(): void{");wr.newLine();
        wr.write("        parent::setUp();");wr.newLine();
        wr.write("        $this->artisan('migrate');");wr.newLine();
        wr.write("        $repo = new "+repositoryClassName(entity())+"();");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("        $this->"+a.name().replace("Id", "")+"Repo = new "+Entity.ucFirst(a.name().replace("Id", ""))+"Repository();");wr.newLine();
            }
        }
        wr.write("        $this->repo = $repo;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * @test");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    public function test_make(){");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("        $"+a.name()+" = $this->"+a.name().replace("Id", "")+"Repo->model()::inRandomOrder()->first()->id;");wr.newLine();
            }
        }
        if(hasIndex){
            wr.write("        $item = factory("+modelClassName(entity())+"::class)->make([");wr.newLine();
            for(Attribute a: entity().attributes){
                if(a.hasTag("index")){
                    wr.write("            '"+Entity.snakecased(a.name())+"' => $"+a.name()+",");wr.newLine();
                }
            }    
            wr.write("        ]);");wr.newLine();
        }else{
            wr.write("        $item = factory("+modelClassName(entity())+"::class)->make();");wr.newLine();
        }
        wr.write("        $this->assertTrue(true);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    /** HELPERS FUNCTIONS --------------------------------------------- **/");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function generateSorter(){");wr.newLine();
        wr.write("        $sorter = new Sorter();");wr.newLine();
        wr.write("        $sorter->asc('id');");wr.newLine();
        wr.write("        return $sorter;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function generateFilterer(){");wr.newLine();
        wr.write("        $filterer = new Filterer();");wr.newLine();
        wr.write("        $filterer->between('id',[0,14]);");wr.newLine();
        wr.write("        return $filterer;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function generateItems($toGenerateCount){");wr.newLine();
        wr.write("        $count = $this->repository()->count()->response();");wr.newLine();
        wr.write("        $generatedItemsId = [];");wr.newLine();
        wr.write("        for($i=$count; $i<($toGenerateCount+$count); $i++){");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("            $"+a.name()+" = $this->"+a.name().replace("Id", "")+"Repo->model()::inRandomOrder()->first()->id;");wr.newLine();
            }
        }
        if(hasIndex){
            wr.write("            $data = factory("+modelClassName(entity())+"::class)->make([");wr.newLine();
            for(Attribute a: entity().attributes){
                if(a.hasTag("index")){
                    wr.write("                '"+Entity.snakecased(a.name())+"' => $"+a.name()+",");wr.newLine();
                }
            }    
            wr.write("            ]);");wr.newLine();
        }else{
            wr.write("            $data = factory("+modelClassName(entity())+"::class)->make();");wr.newLine();
        }
        wr.write("            $item = $this->repository()->create($data);");wr.newLine();
        wr.write("            $generatedItemsId[] = $item->response()->id;");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        $this->assertEquals(count($generatedItemsId), $toGenerateCount);");wr.newLine();
        wr.write("        return $generatedItemsId;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}   ");wr.newLine();
        wr.write("");wr.newLine();
        wr.close();
    }
    
}
