/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xolens.codhelp.writer.pglaravel.test;

import com.xolens.codhelp.property.Attribute;
import com.xolens.codhelp.property.Entity;
import com.xolens.codhelp.writer.pglaravel.LaravelEntityWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author cedrick
 */
public class RepositoryViewTestWriter extends LaravelEntityWriter {
        
    public RepositoryViewTestWriter(Entity e){
        super(e);
    }
    
    @Override
    public String namespace() {
        return viewRepositoryTestNamespase();
    }

    @Override
    public String outputDir() {
        return value("dir.src.namespace.test.repository.view");
    }    
    
    @Override
    public String className() {
        return viewRepositoryTestClassName(entity());
    }
    
    @Override
    public void write() throws IOException {
        if(entity()==null) return;
        
        BufferedWriter wr = fileWriter();
        
        wr.write("<?php");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("namespace "+namespace()+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("use "+viewRepositoryNamespase(entity())+";");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Util\\Model\\Sorter;");wr.newLine();
        wr.write("use Xolens\\PgLarautil\\App\\Util\\Model\\Filterer;");wr.newLine();
        wr.write("use "+value("namespace.test")+"\\"+className("class.php.test.writableTestBase")+";");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("final class "+className()+" extends "+className("class.php.test.writableTestBase"));wr.newLine();
        wr.write("{");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * Setup the test environment.");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    protected function setUp(): void{");wr.newLine();
        wr.write("        parent::setUp();");wr.newLine();
        wr.write("        $this->artisan('migrate');");wr.newLine();
        wr.write("        $this->repo = new "+viewRepositoryClassName(entity())+"();");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    /**");wr.newLine();
        wr.write("     * @test");wr.newLine();
        wr.write("     */");wr.newLine();
        wr.write("    public function test_make(){");wr.newLine();
        wr.write("        $i = rand(0, 10000);");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("        $"+a.name()+" = $this->"+a.name().replace("Id", "")+"Repo->model()::inRandomOrder()->first()->id;");wr.newLine();
            }
        }
        wr.write("        $item = $this->repository()->make([");wr.newLine();
        for(Attribute a: entity().attributes){
            if(!a.name().equals("id")){
                if(a.hasTag("index")){
                    wr.write("            '"+Entity.snakecased(a.name())+"' => $"+a.name()+",");wr.newLine();
                  }else{
                    wr.write("            '"+Entity.snakecased(a.name())+"' => '"+a.name()+"'.$i,");wr.newLine();
                }
            }
        }
        wr.write("        ]);");wr.newLine();
        wr.write("        $this->assertTrue(true);");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("    ");wr.newLine();
        wr.write("    /** HELPERS FUNCTIONS --------------------------------------------- **/");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function generateSorter(){");wr.newLine();
        wr.write("        $sorter = new Sorter();");wr.newLine();
        wr.write("        $sorter->asc('id');");wr.newLine();
        wr.write("        return $sorter;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function generateFilterer(){");wr.newLine();
        wr.write("        $filterer = new Filterer();");wr.newLine();
        wr.write("        $filterer->between('id',[0,14]);");wr.newLine();
        wr.write("        return $filterer;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("");wr.newLine();
        wr.write("    public function generateItems($toGenerateCount){");wr.newLine();
        wr.write("        $count = $this->repository()->count()->response();");wr.newLine();
        wr.write("        $generatedItemsId = [];");wr.newLine();
        wr.write("        ");wr.newLine();
        wr.write("        for($i=$count; $i<($toGenerateCount+$count); $i++){");wr.newLine();
        for(Attribute a: entity().attributes){
            if(a.hasTag("index")){
                wr.write("            $"+a.name()+" = $this->"+a.name().replace("Id", "")+"Repo->model()::inRandomOrder()->first()->id;");wr.newLine();
            }
        }
        wr.write("            $item = $this->repository()->create([");wr.newLine();
        for(Attribute a: entity().attributes){
            if(!a.name().equals("id")){
                if(a.hasTag("index")){
                    wr.write("                '"+Entity.snakecased(a.name())+"' => $"+a.name()+",");wr.newLine();
                  }else{
                    String val = "";
                    switch(a.type().toLowerCase()){
                        case "int":
                        case "integer":
                            val = "random_int(0,400000)";
                            break;
                        case "date":
                            val = "'10-10-'.random_int(1987,2018)";
                            break;
                        case "bool":
                        case "boolean":
                            val = "$i%2==0";
                            break;
                        case "string":
                        case "text":
                        case "enum":
                        default:
                            val = "'"+a.name()+"'.$i";
                            break;
                    }
                    wr.write("                '"+Entity.snakecased(a.name())+"' => "+val+",");wr.newLine();
                }
            }
        }
        wr.write("            ]);");wr.newLine();
        wr.write("            $generatedItemsId[] = $item->response()->id;");wr.newLine();
        wr.write("        }");wr.newLine();
        wr.write("        $this->assertEquals(count($generatedItemsId), $toGenerateCount);");wr.newLine();
        wr.write("        return $generatedItemsId;");wr.newLine();
        wr.write("    }");wr.newLine();
        wr.write("}   ");wr.newLine();
        wr.write("");wr.newLine();
        wr.close();
    }
    
}
